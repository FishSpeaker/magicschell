/* Copyright (c) 2009 Jason Pratt
 * This software is distributed under the MIT License, which can be found
 * in the LICENSE.txt file accompanying the software, or at this url:
 * http://www.opensource.org/licenses/mit-license.html
 */

function updateValidation( isValid, inputID, errorNotificationID ) {
	var wasValid = $("#" + errorNotificationID).is( ":hidden" )
	if( wasValid != isValid ) {
		if( isValid ) {
			$("#" + errorNotificationID).hide()
			inputStyle = $("#" + inputID)[0].style
			inputStyle.borderColor = inputStyle.borderWidth = inputStyle.borderStyle = ""
		} else {
			$("#" + errorNotificationID).show()
			$("#" + inputID).css( "border", "2px solid red" )
		}
	}
}

function checkIntRange( low, high, inputID, errorNotificationID, allowEmpty ) {
	var value = $("#" + inputID)[0].value
	var intValue = parseInt( value )
	var isValid = (allowEmpty && (value == "")) || (!isNaN( intValue ) && (intValue >= low) && (intValue <= high))
	updateValidation( isValid, inputID, errorNotificationID )
	return isValid
}

function checkFloat( inputID, errorNotificationID, allowEmpty ) {
	var value = $("#" + inputID)[0].value
	var floatValue = parseFloat( value )
	var isValid = (allowEmpty && (value == "")) || !isNaN( floatValue )
	updateValidation( isValid, inputID, errorNotificationID )
	return isValid
}

function checkPositiveFloat( inputID, errorNotificationID, allowEmpty ) {
	var value = $("#" + inputID)[0].value
	var floatValue = parseFloat( value )
	var isValid = (allowEmpty && (value == "")) || (!isNaN( floatValue ) && (floatValue >= 0.0))
	updateValidation( isValid, inputID, errorNotificationID )
	return isValid
}

function checkStrictlyPositiveFloat( inputID, errorNotificationID, allowEmpty ) {
	var value = $("#" + inputID)[0].value
	var floatValue = parseFloat( value )
	var isValid = (allowEmpty && (value == "")) || (!isNaN( floatValue ) && (floatValue > 0.0))
	updateValidation( isValid, inputID, errorNotificationID )
	return isValid
}

function hideAndShow( hideSelector, showSelector ) {
	$(hideSelector).hide()
	$(showSelector).show()
}

function daysInMonth( year, month ) {
	return 32 - new Date( year, month, 32 ).getDate();
}

function updateDayCombo( yearCombo, monthCombo, dayCombo ) {
	var currentYear = yearCombo.val();
	var currentMonth = monthCombo.val()
	var currentDay = dayCombo.val()

	var numDays = 31;
	if( currentYear == "" ) {
		currentYear = "2009"
	}
	if( currentMonth != "" ) {
		numDays = daysInMonth( currentYear, currentMonth - 1 )
	}

	var options = "";
	if( currentDay == "" ) {
		options += '<option value="" selected="selected"></option>\n'
	} else {
		options += '<option value=""></option>\n'
	}
	for( var i = 1; i <= numDays; i++ ) {
		if( parseInt( currentDay ) == i ) {
			options += '<option value="' + i + '" selected="selected">' + i + '</option>'
		} else {
			options += '<option value="' + i + '">' + i + '</option>'
		}
	}
	dayCombo.html( options )
}

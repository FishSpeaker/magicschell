# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

from google.appengine.ext.webapp import template
register = template.create_template_register()

# This is a slight modification to Django block tags.  It allows blocks to
# be filled using context variables as well as simply being overridden in
# sub-templates.
def alternateBlockRender( self, context ):
	BLOCK_CONTEXT_KEY = template.django.template.loader_tags.BLOCK_CONTEXT_KEY
	BlockNode = template.django.template.loader_tags.BlockNode

	block_context = context.render_context.get(BLOCK_CONTEXT_KEY)
	context.push()
	if block_context is None:
		context['block'] = self
		if self.name in context:
			result = context[self.name]
		else:
			result = self.nodelist.render(context)
	else:
		push = block = block_context.pop(self.name)
		if block is None:
			block = self
		# Create new block so we can store context without thread-safety issues.
		block = BlockNode(block.name, block.nodelist)
		block.context = context
		context['block'] = block
		if self.name in context:
			result = context[self.name]
		else:
			result = block.nodelist.render(context)
		if push is not None:
			block_context.push(self.name, push)
	context.pop()
	return result

def installAlternateBlockRender():
	template.django.template.loader_tags.BlockNode.render = alternateBlockRender

# Copyright (c) 2009-2010 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import datetime
import operator
import random
import traceback

from google.appengine.ext import db
from google.appengine.api import users

from BaseHandler import BaseHandler
from models import PlayerTeamAssociation
from models import Team
from models import Tournament
import util

class AdminTournamentHandler( BaseHandler ):
	def getOrPost( self ):
		if users.get_current_user() is None:
			self.redirect( users.create_login_url( self.request.uri ) )
		else:
			self.baseInit()
			self.templateContent["title"] = "Administer Tournament"

			# Get tournament.
			tournament = None
			if "key" in self.request.params:
				tournamentKey = self.request.params["key"]
				try:
					tournament = db.get( db.Key( tournamentKey ) )
					if not isinstance( tournament, Tournament ):
						tournament = None
						errorMessage = "Key (%s) does not represent a Tournament." % tournamentKey
				except db.BadKeyError:
					errorMessage = "Illegal Tournament key: %s." % tournamentKey
			else:
				tournament = util.getCurrentTournament()
				if tournament is None:
					errorMessage = "No default Tournament found."

			# Check for authorization
			if tournament is None:
				self.respondWithError( errorMessage )
			else:
				if util.isTournamentAdmin( users.get_current_user(), tournament ) or users.is_current_user_admin():
					# Process page
					self.templateContent["tournamentName"] = tournament.name
					if self.request.get( "submitRoundInfo" ) == "true":
						params, errors = self.validateRoundInfoParams()
						self.templateContent["errors"] = errors
						if len( errors ) == 0:
							# Check for start of ladder stage
							if (tournament.currentStage != "ladder") and (params["tournamentKeywords"]["currentStage"] == "ladder"):
								tournament.ladderStartDate = datetime.date.today()

							# Update tournament.
							for property, value in params["tournamentKeywords"].items():
								setattr( tournament, property, value )

							tournament.put()

							# Redirect to Leaderboard.
							self.redirect( "/" )
						else:
							self.fillForms( self.getParamsFromDatabase( tournament ), self.request.params )
							self.respondWithTemplate( "AdminTournament.tmpl" )
					elif self.request.get( "submitBoostersReceived" ) == "true":
						params, errors = self.validateBoostersReceivedParams( tournament )
						self.templateContent["errors"] = errors
						if len( errors ) == 0:
							# Update boostersReceived
							team = params["team"]
							team.boostersReceived = params["boostersReceived"]
							team.put()

							# Redirect to Leaderboard.
							self.redirect( "/" )
						else:
							self.fillForms( self.getParamsFromDatabase( tournament ), self.request.params )
							self.respondWithTemplate( "AdminTournament.tmpl" )
					elif self.request.get( "recalculateAllRatings" ) == "true":
						try:
							util.recalculateAllRatings( tournament )

							# Redirect to Leaderboard.
							self.redirect( "/" )
						except util.RatingsLockedError:
							self.templateContent["errors"] = ["Ratings are already being recalculated."]
							self.fillForms( self.getParamsFromDatabase( tournament ), self.request.params )
							self.respondWithTemplate( "AdminTournament.tmpl" )
					elif self.request.get( "clearUniqueLadderOpponentsCache" ) == "true":
						util.clearUniqueLadderOpponentsCache( tournament )
						# Redirect to Leaderboard.
						self.redirect( "/" )
					elif self.request.get( "clearTeamLadderMatchesCountCache" ) == "true":
						util.clearTeamLadderMatchesCountCache( tournament )
						# Redirect to Leaderboard.
						self.redirect( "/" )
					elif self.request.get( "clearPlayerLadderMatchesCountCache" ) == "true":
						util.clearPlayerLadderMatchesCountCache( tournament )
						# Redirect to Leaderboard.
						self.redirect( "/" )
#					elif self.request.get( "createRandomMatches" ) == "true":
#						players = util.getPlayersInTournament( tournament )
#						for i in range( 100 ):
#							player1 = player2 = random.choice( players )
#							team1 = team2 = util.getTeamFromPlayerAndTournament( player1, tournament )
#							while team1.key() == team2.key():
#								player2 = random.choice( players )
#								team2 = util.getTeamFromPlayerAndTournament( player2, tournament )
#							matchKeywords = {
#								"creator" : users.get_current_user(),
#								"tournament" : tournament,
#								"player1" : player1,
#								"player2" : player2,
#								"player1Score" : float( random.randint( 1, 100 ) ),
#								"player2Score" : float( random.randint( 1, 100 ) ),
#								"isValidForLadder" : True,
#								"isValidForElimination" : False,
#								"team1" : team1,
#								"team2" : team2,
#							}
#							match, matchDuplicate = util.submitMatch( matchKeywords )
#
#						# Redirect to Leaderboard.
#						self.redirect( "/" )
					else:
						self.fillForms( self.getParamsFromDatabase( tournament ), self.request.params )
						self.respondWithTemplate( "AdminTournament.tmpl" )
				else:
					self.respondWithError( "%s is not authorized to administer %s." % (users.get_current_user().email(), tournament.name) )

	def getParamsFromDatabase( self, tournament ):
		reprDicts = []
		for team in tournament.teams:
			reprDict = util.getDisplayReprDict( team )
			reprDict["boostersEarned"] = util.getIntRepr( util.getBoostersEarned( team ) )
			reprDict["boostersOwed"] = util.getIntRepr( util.getBoostersOwed( team ) )
			reprDict["playerReprs"] = ", ".join( sorted( [util.getDisplayReprDict( playerAssociation.player )["repr"] for playerAssociation in team.playerAssociations] ) )
			reprDicts.append( reprDict )
		reprDicts.sort( key=operator.itemgetter( "name" ) )
		if tournament.eliminationStartDatetime is None:
			tournament.eliminationStartDatetime = datetime.datetime.today()
			tournament.put()
		params = {
			"key" : tournament.key(),
			"eliminationYear" : tournament.eliminationStartDatetime.year,
			"eliminationMonth" : tournament.eliminationStartDatetime.month,
			"eliminationDay" : tournament.eliminationStartDatetime.day,
			"eliminationHour" : "%02d" % tournament.eliminationStartDatetime.hour,
			"eliminationMinute" : "%02d" % tournament.eliminationStartDatetime.minute,
			"currentStage" : tournament.currentStage,
			"teams" : reprDicts,
		}
		return params

	def fillForms( self, defaultParams, passedParams ):
		# Setup defaults
		self.templateContent["currentParams"] = defaultParams

		# Overwrite from passedParams.
		self.templateContent["currentParams"].update( passedParams )

		# Generate elimination dropdown selections.
		self.templateContent["eliminationYears"] = range( 2009, datetime.date.today().year + 5 )
		self.templateContent["eliminationDays"] = range( 1, 32 )
		self.templateContent["eliminationMonths"] = util.monthTuples
		self.templateContent["eliminationHours"] = ["%02d" % h for h in range( 0, 24 )]
		self.templateContent["eliminationMinutes"] = ["%02d" % m for m in range( 0, 61, 10 )]

	def validateRoundInfoParams( self ):
		# Input params:
		# -- eliminationYear, eliminationMonth, eliminationDay, eliminationHour, eliminationMinute : valid date/time
		# -- currentStage : string

		# Output params:
		# -- tournamentKeywords : {}

		params = {
			"tournamentKeywords" : {},
		}
		errors = []

		# elimination date/time
		eliminationYearString = self.request.params["eliminationYear"].strip()
		eliminationMonthString = self.request.params["eliminationMonth"].strip()
		eliminationDayString = self.request.params["eliminationDay"].strip()
		eliminationHourString = self.request.params["eliminationHour"].strip()
		eliminationMinuteString = self.request.params["eliminationMinute"].strip()
		if "" in (eliminationYearString, eliminationMonthString, eliminationDayString, eliminationHourString, eliminationMinuteString):
			params["tournamentKeywords"]["eliminationStartDatetime"] = None
		else:
			try:
				eliminationYear = util.parseInt( eliminationYearString )
			except util.ParseError:
				errors.append( "Elimination year must be a number." )
			try:
				eliminationMonth = util.parseInt( eliminationMonthString, minimum=1, maximum=12 )
			except util.ParseError:
				errors.append( "Elimination month must be a number between 1 and 12." )
			try:
				eliminationDay = util.parseInt( eliminationDayString, minimum=1, maximum=31 )
			except util.ParseError:
				errors.append( "Elimination day must be a valid day of the given month." )
			try:
				eliminationHour = util.parseInt( eliminationHourString, minimum=0, maximum=23 )
			except util.ParseError:
				errors.append( "Elimination hour must be a number between 0 and 23." )
			try:
				eliminationMinute = util.parseInt( eliminationMinuteString, minimum=0, maximum=59 )
			except util.ParseError:
				errors.append( "Elimination minute must be a number between 0 and 59." )
			if len( errors ) == 0:
				try:
					params["tournamentKeywords"]["eliminationStartDatetime"] = datetime.datetime( eliminationYear, eliminationMonth, eliminationDay, eliminationHour, eliminationMinute )
				except ValueError:
					errors.append( "Invalid date: year=%s, month=%s, day=%s, hour=%s, minute=%s." % (eliminationYearString, eliminationMonthString, eliminationDayString, eliminationHourString, eliminationMinuteString) )
					params["tournamentKeywords"]["eliminationStartDatetime"] = None

		# currentStage
		currentStageString = self.request.params["currentStage"]
		if currentStageString in ("announced", "signUp", "preLadder", "ladder", "preElimination", "elimination", "finished"):
			params["tournamentKeywords"]["currentStage"] = currentStageString
		else:
			errors.append( 'Invalid currentStage: (%s). Must be one of: "announced", "signUp", "preLadder", "ladder", "preElimination", "elimination", "finished".' )
			params["tournamentKeywords"]["currentStage"] = None

		return params, errors

	def validateBoostersReceivedParams( self, tournament ):
		# Input params:
		# -- teamKey : valid team in tournament
		# -- boostersReceived : int >= 0

		# Output params:
		# -- team : team
		# -- boostersReceived : boostersReceived

		params = {
			"team" : None,
			"boostersReceived" : None,
		}
		errors = []

		teamKey = None
		if "teamKey" in self.request.params:
			teamKey = self.request.params["teamKey"]
			try:
				params["team"] = db.get( db.Key( teamKey ) )
				if not isinstance( params["team"], Team ):
					params["team"] = None
					errors.append( "Key (%s) does not represent a Team." % teamKey )
				if params["team"].tournament.key() != tournament.key():
					errors.append( "team.tournament: (%s); tournament: (%s)." % (params["team"].tournament.key(), tournament.key()) )
					params["team"] = None
					errors.append( "Team (%s) is not in Tournament (%s)." % (teamKey, tournament.key()) )
			except db.BadKeyError:
				errors.append( "Illegal Team key: %s." % teamKey )
		else:
			errorMessage = "No teamKey given."

		# boostersReceived
		try:
			params["boostersReceived"] = util.parseInt( self.request.params["boostersReceived"], minimum=0 )
		except util.ParseError:
			errors.append( "Boosters received must be a non-negative integer." )

		return params, errors





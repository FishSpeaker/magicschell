# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import operator

from google.appengine.ext import db

from handlers.BaseHandler import BaseHandler
from models import Tournament
import util

class LeaderboardHandler( BaseHandler ):
	def getOrPost( self ):
		self.baseInit()
		self.templateContent["title"] = "Leaderboard"
		params, errors = self.validateParams()
		self.templateContent.update( params )
		self.templateContent["errors"] = errors
		self.respondWithTemplate( "Leaderboard.tmpl" )

	def validateParams( self ):
		# Input params:
		# -- tab : string (teams|individuals)
		# -- selectedTournament : tournamentKey

		# Output params:
		# -- currentParams : {tab, selectedTournament}
		# -- tournamentName : string
		# -- adminsInfo : [(adminLink, adminRepr), ...]
		# -- teamInfos : [{rank, teamLink, teamRepr, playerInfos, gamesPlayed, boostersEarned, boostersOwed, boostersReceived, rating}, ...]
		#                                         -- playerInfos : [{playerLink, shortRepr}, ...]
		# -- playerInfos : [{rank, playerLink, playerRepr, teamLink, teamRepr, gamesPlayed, uniqueOpponents, rating}, ...]

		params = {}
		errors = []

		## Input parameters

		# tab
		if "tab" in self.request.params:
			tab = self.request.params["tab"].strip().lower()
			if tab not in ("teams", "individuals"):
				tab = "teams"
		else:
			tab = "teams"

		# selectedTournament
		selectedTournament = None
		if "selectedTournament" in self.request.params:
			tournamentKey = self.request.params["selectedTournament"]
			try:
				selectedTournament = db.get( db.Key( tournamentKey ) )
				if not isinstance( selectedTournament, Tournament ):
					selectedTournament = None
					errors.append( "Selected tournament key (%s) does not represent a Tournament." % tournamentKey )
			except db.BadKeyError:
				errors.append( "Selected tournament key (%s) is invalid." % tournamentKey )

		if selectedTournament is None:
			selectedTournament = util.getCurrentTournament()

		# Store current tournament in player settings.
		currentPlayer = util.getCurrentPlayer()
		if (currentPlayer is not None) and (selectedTournament is not None):
			util.setCurrentTournament( currentPlayer, selectedTournament )

		## Output parameters
		if selectedTournament is None:
			params["noData"] = True
			return params, errors
		else:
			params["noData"] = False

		# currentParams : {tab, selectedTournament}
		params["currentParams"] = {"tab" : tab,
		                           "selectedTournament" : str( selectedTournament.key() )}

		# tournamentName : string
		params["tournamentName"] = selectedTournament.name

		# adminsInfo : [(adminLink, adminRepr), ...]
		adminsInfo = []
		for user in selectedTournament.admins:
			player = util.getOrCreatePlayer( user )
			adminsInfo.append( util.getPlayerLinkAndRepr( player ) )
		params["adminsInfo"] = adminsInfo

		if tab == "teams":
			# -- teamInfos : [{rank, teamLink, teamRepr, playerInfos, gamesPlayed, boostersEarned, boostersReceived, rating}, ...]
			#                                         -- playerInfos : [{playerLink, shortRepr}, ...]
			teamInfos = []
			rank = rankCounter = 1
			lastRating = -1
			for team in sorted( selectedTournament.teams, key=operator.attrgetter( "rating" ), reverse=True ):
				if team.rating != lastRating:
					rank = rankCounter
				lastRating = team.rating
				teamLink, teamRepr = util.getTeamLinkAndRepr( team )
				playerInfos = []
				for player in [playerAssociation.player for playerAssociation in team.playerAssociations.fetch( 1000 )]:
					playerLink, shortRepr = util.getPlayerLinkAndShortRepr( player )
					playerInfos.append( {"playerLink" : playerLink, "shortRepr" : shortRepr} )
				playerInfos.sort( key=operator.itemgetter( "shortRepr" ) )
				teamInfo = {
					"rank" : rank,
					"teamLink" : teamLink,
					"teamRepr" : teamRepr,
					"playerInfos" : playerInfos,
					"gamesPlayed" : util.getTeamNumLadderMatches( team ),
					"uniqueOpponents" : util.getTeamNumUniqueOpponents( team ),
					"boostersEarned" : util.getBoostersEarned( team ),
					"boostersOwed" : util.getBoostersOwed( team ),
					"boostersReceived" : team.boostersReceived,
					"rating": "%.0f" % team.rating,
				}
				rankCounter += 1
				teamInfos.append( teamInfo )
			params["teamInfos"] = teamInfos
		elif tab == "individuals":
			# playerInfos : [{rank, playerLink, playerRepr, gamesPlayed, rating}, ...]
			players = util.getPlayersInTournament( selectedTournament, sort="rating" )
			playerInfos = []
			rank = rankCounter = 1
			lastRating = -1
			for player in players:
				playerRating = util.getOrCreatePlayerRating( player, selectedTournament )
				if playerRating.rating != lastRating:
					rank = rankCounter
				lastRating = playerRating.rating
				playerLink, playerRepr = util.getPlayerLinkAndRepr( player )
				team = util.getTeamFromPlayerAndTournament( player, selectedTournament )
				if team is None:
					logging.error( "Null team encountered for player, tournament: %s, %s" % (player.key(), selectedTournament.key()) )
					continue
				playerInfo = {
					"rank" : rank,
					"playerLink" : playerLink,
					"playerRepr" : playerRepr,
					"teamLink" : util.getTeamLinkAndRepr( team )[0],
					"teamName" : team.name,
					"gamesPlayed" : util.getPlayerNumLadderMatches( player, selectedTournament ),
					"uniqueOpponents" : util.getNumUniqueLadderOpponents( player, selectedTournament ),
					"rating": "%.0f" % playerRating.rating,
				}
				rankCounter += 1
				playerInfos.append( playerInfo )
			params["playerInfos"] = playerInfos

		return params, errors

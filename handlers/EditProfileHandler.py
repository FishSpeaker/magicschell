# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import datetime
import re

from google.appengine.ext import db
from google.appengine.api import users

from BaseHandler import BaseHandler
from models import Player
import util

class EditProfileHandler( BaseHandler ):
	def getOrPost( self ):
		if users.get_current_user() is None:
			self.redirect( users.create_login_url( self.request.uri ) )
		else:
			self.baseInit()
			self.templateContent["title"] = "Edit Profile"

			# Get player to edit.
			player = util.getOrCreatePlayer( users.get_current_user() )

			if self.request.get( "submitProfile" ) == "true":
				# New player info submitted.
				params, errors = self.validateParams()
				self.templateContent["errors"] = errors
				if len( errors ) == 0:
					# Update player.
					for property, value in params["playerKeywords"].items():
						setattr( player, property, value )
					player.put()

					# Redirect to Player page
#					self.redirect( "/viewPlayer?playerKey=%s" % player.key() )
					self.redirect( "/" )
				else:
					# Errors encountered.
					self.fillForm( self.request.params )
					self.respondWithTemplate( "EditProfile.tmpl" )
			else:
				# Fill form with current player data.
				params = {
					"firstName" : util.getEmptyStringForNone( player.firstName ),
					"lastName" : util.getEmptyStringForNone( player.lastName ),
					"email" : util.getEmptyStringForNone( player.email ),
				}

				self.fillForm( params )
				self.respondWithTemplate( "EditProfile.tmpl" )

	def fillForm( self, params ):
		# Setup defaults
		self.templateContent["currentParams"] = {}

		# Overwrite from self.request.params.
		self.templateContent["currentParams"].update( params )

	def validateParams( self ):
		# Input params:
		# -- firstName : string
		# -- lastName : string
		# -- email : string

		# Output params:
		# -- playerKeywords : {}

		playerKeywords = {}
		errors = []

		# firstName
		playerKeywords["firstName"] = self.request.params["firstName"].strip()
		# lastName
		playerKeywords["lastName"] = self.request.params["lastName"].strip()
		# email
		playerKeywords["email"] = db.Email( self.request.params["email"].strip() )

		return {"playerKeywords" : playerKeywords}, errors






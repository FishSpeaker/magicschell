# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging

from google.appengine.ext import db
from google.appengine.api import users

from BaseHandler import BaseHandler
from models import PlayerTeamAssociation
from models import Team
import util

class AddOrEditTournamentHandler( BaseHandler ):
	def updateTeamInfo( self, tournament, teamNamesToUsers ):
		# Remove old Teams and PlayerTeamAssociations
		for team in tournament.teams:
			for playerAssociation in team.playerAssociations:
				playerAssociation.delete()
			team.delete()

		# Create new Teams and PlayerTeamAssociations
		for teamName, _users in teamNamesToUsers.items():
			team = Team( name=teamName,
			             tournament=tournament,
			             rating=0.0,
			             boostersReceived=0 )
			team.put()
			for user in _users:
				playerTeamAssociation = PlayerTeamAssociation( team=team,
				                                               player=util.getOrCreatePlayer( user ) )
				playerTeamAssociation.put()

			# Calculate team rating.
			util.updateTeamRating( team )

	def fillForm( self, params ):
		# Setup defaults
		self.templateContent["currentParams"] = {
			"adminEmails" : users.get_current_user().email(),
			"playersPerTeam" : "2",
			"maxBoostersPerTeam" : "10",
			"opponentsPerBooster" : "2",
			"teamInfo" : "",
			"initialRating" : "1500",
			"maxRatingChange" : "25",
			"scale" : "400",
		}

		# Overwrite from passed params.
		self.templateContent["currentParams"].update( params )

	def validateParams( self ):
		# Input params:
		# -- name : string (non-zero length)
		# -- description : string
		# -- adminEmails : string (one email per line)
		# -- maxBoostersPerTeam : strictly positive integer
		# -- opponentsPerBooster : strictly positive integer
		# -- playersPerTeam : strictly positive integer
		# -- teamInfo : multiline team info string
		# -- initialRating : float
		# -- scale : float
		# -- maxRatingChange : float

		# Output params:
		# -- tournamentKeywords : {}
		# -- teamNamesToUsers : { teamName : [user1, user2, user3] }

		params = {
			"tournamentKeywords" : {},
			"teamNamesToUsers" : {},
		}
		errors = []

		# name
		name = self.request.params["name"].strip()
		if len( name ) == 0:
			errors.append( "No Tournament Name supplied." )
		else:
			params["tournamentKeywords"]["name"] = name
		# description
		params["tournamentKeywords"]["description"] = self.request.params["description"].strip()

		# adminEmails
		emails = self.request.params["adminEmails"].strip().split()
		params["tournamentKeywords"]["admins"] = [users.User( email=email.strip() ) for email in emails]

		# maxBoostersPerTeam
		try:
			params["tournamentKeywords"]["maxBoostersPerTeam"] = util.parseInt( self.request.params["maxBoostersPerTeam"], minimum=1 )
		except util.ParseError:
			errors.append( "Max Boosters Per Team must be a positive integer." )

		# opponentsPerBooster
		try:
			params["tournamentKeywords"]["opponentsPerBooster"] = util.parseInt( self.request.params["opponentsPerBooster"], minimum=1 )
		except util.ParseError:
			errors.append( "Unique Opponents Per Booster must be a positive integer." )

		# playersPerTeam
		playersPerTeam = None
		try:
			playersPerTeam = util.parseInt( self.request.params["playersPerTeam"], minimum=1 )
			params["tournamentKeywords"]["playersPerTeam"] = playersPerTeam
		except util.ParseError:
			errors.append( "Players Per Team must be a positive integer." )

		# teamInfo
		if playersPerTeam is not None:
			try:
				for line in self.request.params["teamInfo"].splitlines():
					if len( line.strip() ) > 0:
						parts = line.strip().split( "|" )
						teamName = parts[0]
						emails = parts[1:]
						if len( emails ) == playersPerTeam:
							params["teamNamesToUsers"][teamName] = [users.User( email=email.strip() ) for email in emails]
						else:
							errors.append( "Error parsing teamInfo: incorrect number of emails: %s" % line )
			except:
				errors.append( "Error parsing teamInfo: %s" % traceback.format_exc().splitlines()[-1] )
		if len( params["teamNamesToUsers"] ) < 1:
			errors.append( "No Team Info supplied." )

		# initialRating
		try:
			params["tournamentKeywords"]["initialRating"] = util.parseFloat( self.request.params["initialRating"] )
		except util.ParseError:
			errors.append( "Initial Rating must be a number." )
		# scale
		try:
			params["tournamentKeywords"]["scale"] = util.parseFloat( self.request.params["scale"] )
		except util.ParseError:
			errors.append( "Scale must be a number." )
		# maxRatingChange
		try:
			params["tournamentKeywords"]["maxRatingChange"] = util.parseFloat( self.request.params["maxRatingChange"] )
		except util.ParseError:
			errors.append( "Max Rating Change must be a number." )

		return params, errors






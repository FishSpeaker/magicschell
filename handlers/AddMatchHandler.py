# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging

from google.appengine.api import users

from handlers.AddOrEditMatchHandler import AddOrEditMatchHandler
from models import Match
import util

class AddMatchHandler( AddOrEditMatchHandler ):
	def getOrPost( self ):
		if users.get_current_user() is None:
			self.redirect( users.create_login_url( self.request.uri ) )
		else:
			self.baseInit()
			self.templateContent["title"] = "Submit Match"
			tournament = util.getCurrentTournament()

			if not util.isUserInTournament( users.get_current_user(), tournament ):
				self.respondWithError( "User (%s) is not registered for the current tournament." % users.get_current_user().email() )
			else:
				currentPlayer = util.getCurrentPlayer()
				bracketOpponent = util.getBracketOpponent( currentPlayer, tournament )
				if (tournament.currentStage == "elimination") and (bracketOpponent is None):
					self.respondWithError( "Could not find a valid bracket opponent for %s" % util.getDisplayReprDict( currentPlayer )["repr"] )
				elif self.request.get( "submitMatch" ) == "true":
					params, errors = self.validateParams()
					self.templateContent["errors"] = errors
					if len( errors ) == 0:
						# Add final pieces to matchKeywords.
						params["matchKeywords"]["creator"] = users.get_current_user()
						params["matchKeywords"]["isValidForLadder"] = params["matchKeywords"]["tournament"].currentStage == "ladder"
						params["matchKeywords"]["isValidForElimination"] = params["matchKeywords"]["tournament"].currentStage == "elimination"
						params["matchKeywords"]["team1"] = util.getTeamFromPlayerAndTournament( params["matchKeywords"]["player1"], params["matchKeywords"]["tournament"] )
						params["matchKeywords"]["team2"] = util.getTeamFromPlayerAndTournament( params["matchKeywords"]["player2"], params["matchKeywords"]["tournament"] )

						# Submit match
						try:
							match, match2 = util.submitMatch( params["matchKeywords"] )

							if params["matchKeywords"]["isValidForElimination"]:
								# Redirect to Bracket.
								self.redirect( "/bracket" )
							else:
								# Redirect to Leaderboard.
								self.redirect( "/" )
						except util.RatingsLockedError:
							self.templateContent["errors"] = ["Match submission is currently locked due to maintenance.  Please submit again in a few minutes."]
							self.fillForm( self.request.params )
							self.respondWithTemplate( "AddOrEditMatch.tmpl" )
					else:
						self.fillForm( self.request.params )
						self.respondWithTemplate( "AddOrEditMatch.tmpl" )
				else:
					self.fillForm( self.request.params, bracketOpponent=bracketOpponent )
					self.respondWithTemplate( "AddOrEditMatch.tmpl" )




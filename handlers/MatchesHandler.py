# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import operator

from google.appengine.ext import db
from google.appengine.api import users

from handlers.BaseHandler import BaseHandler
from handlers.PagingHandler import PagingHandler
from models import Player
import util

class MatchesHandler( BaseHandler, PagingHandler ):
	def getOrPost( self ):
		self.baseInit()
		self.templateContent["title"] = "Matches"
		self.templateContent["isAdmin"] = util.isTournamentAdmin( users.get_current_user(), util.getCurrentTournament() )
		params, errors = self.validateParams()
		self.templateContent.update( params )
		self.templateContent["errors"] = errors
		self.respondWithTemplate( "Matches.tmpl" )

	def validateParams( self ):
		# Input params:
		# -- resultsPerPage : int (10-100)
		# -- startIndex : int (1-991)

		# Output params:
		# -- currentParams : {resultsPerPage, startIndex}
		# -- tournamentName : string
		# -- matchInfos : [{dateSubmitted,
		#                   timeSubmitted,
		#                   type,
		#                   winner,
		#                   player1Link,
		#                   player1Repr,
		#                   team1Link,
		#                   team1Repr,
		#                   player1Score,
		#                   player2Link,
		#                   player2Repr,
		#                   team2Link,
		#                   team2Repr,
		#                   player2Score,
		#                   matchKey}, ...]
		# -- pagingBar : <html>
		# -- resultsPerPageWidget : <html>

		params = {}
		errors = []
		tournament = util.getCurrentTournament()
		currentPlayerSettings = util.getCurrentPlayerSettings()

		## Input parameters
		resultsPerPage, startIndex = self.validateResultsPerPageAndStartIndex( self.request, currentPlayerSettings )

		## Output parameters
		baseLink = ""

		# currentParams : {resultsPerPage, startIndex}
		params["currentParams"] = {"resultsPerPage" : resultsPerPage,
		                           "startIndex" : startIndex}

		# tournamentName
		params["tournamentName"] = tournament.name

		# matchInfos
		query = tournament.matches.filter( "isDuplicate =", False ).order( "-datetimeSubmitted" )
		queryCount = query.count()
		params["matchInfos"] = util.getMatchInfos( query, queryCount, startIndex, resultsPerPage )

		# pagingBar
		pageLink = "?startIndex=%d"
		if "resultsPerPage" not in currentPlayerSettings:
			pageLink += "&resultsPerPage=%d" % resultsPerPage

		params["pagingBar"] = util.getPagingBar( queryCount, resultsPerPage, startIndex, pageLink )

		# resultsPerPageWidget
		selectName = "resultsPerPage"
		currentPlayer = util.getCurrentPlayer()
		newPageLink = "?resultsPerPage="
		params["resultsPerPageWidget"] = util.getResultsPerPageWidget( selectName, resultsPerPage, currentPlayer, newPageLink )

		return params, errors

# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import operator
import random
import os

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext.webapp import template

from BaseHandler import BaseHandler
from models import Tournament
import util

class ViewHandler( BaseHandler ):
	def _getOrPost( self, title, template ):
		self.baseInit()
		self.templateContent["title"] = title
		params, errors = self.validateParams()
		self.templateContent.update( params )
		self.templateContent["errors"] = errors
		self.respondWithTemplate( template )

	def getCurrentPlayerInfo( self ):
		currentPlayer = util.getCurrentPlayer()
		if currentPlayer is not None:
			currentPlayerSettings = util.getOrCreatePlayerSettings( currentPlayer )
			currentPlayerTournaments = sorted( [rating.ladder for rating in currentPlayer.ratings], key=operator.attrgetter( "name" ) )
		else:
			currentPlayerSettings = {}
			currentPlayerTournaments = []
		return currentPlayer, currentPlayerSettings, currentPlayerTournaments

	def validateTab( self, request ):
		if "tab" in request.params:
			tab = request.params["tab"].strip().lower()
			if tab not in ("ratings", "matches"):
				tab = "ratings"
		else:
			tab = "ratings"
		return tab

	def validateResultsPerPage( self, request, currentPlayerSettings ):
		if "resultsPerPage" in request.params:
			try:
				resultsPerPage = util.parseInt( request.params["resultsPerPage"], minimum=10, maximum=100, allowBlank=False )
			except util.ParseError:
				if "resultsPerPage" in currentPlayerSettings:
					resultsPerPage = currentPlayerSettings["resultsPerPage"]
				else:
					resultsPerPage = 10
		else:
			if "resultsPerPage" in currentPlayerSettings:
				resultsPerPage = currentPlayerSettings["resultsPerPage"]
			else:
				resultsPerPage = 10
		return resultsPerPage

	def validateStartMatch( self, request, resultsPerPage ):
		if "startMatch" in request.params:
			try:
				startMatch = util.parseInt( request.params["startMatch"], minimum=1, maximum=991, allowBlank=False )
				startMatch = startMatch - ((startMatch - 1) % resultsPerPage)
			except util.ParseError:
				startMatch = 1
		else:
			startMatch = 1
		return startMatch

	def getMatchInfos( self, query, startMatch, resultsPerPage ):
		matchInfos = []
		queryCount = query.count()
		if startMatch > queryCount:
			startMatch = max( queryCount - (queryCount % resultsPerPage) + 1, 1 )
		for match in query.fetch( resultsPerPage, offset=startMatch - 1 ):
			reprDict = util.getDisplayReprDict( match )
			if match.player1Score > match.player2Score:
				winner = "player1"
			elif match.player2Score > match.player1Score:
				winner = "player2"
			else:
				winner = "tie"
			matchInfos.append( {"dateSubmitted" : reprDict["dateSubmitted"],
			                    "timeSubmitted" : reprDict["timeSubmitted"],
			                    "winner" : winner,
			                    "player1Link" : "/viewPlayers?selectedPlayer=%s" % reprDict["player1"]["key"],
			                    "player1Repr" : reprDict["player1"]["repr"],
			                    "player1Score" : reprDict["player1Score"],
			                    "player2Link" : "/viewPlayers?selectedPlayer=%s" % reprDict["player2"]["key"],
			                    "player2Repr" : reprDict["player2"]["repr"],
			                    "player2Score" : reprDict["player2Score"] } )
		return matchInfos, queryCount

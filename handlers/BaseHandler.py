# Copyright (c) 2009-2010 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import os.path

from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

import util

class BaseHandler( webapp.RequestHandler ):
	def baseInit( self ):
		self.currentTournament = util.getCurrentTournament()
		self.templateContent = {
			"accountBar" : self.getAccountBar(),
			"actionBar" : self.getActionBar(),
			"title" : "Untitled",
			"tournamentName" : self.getTournamentName(),
			"tournamentExtraInfo" : self.getTournamentExtraInfo(),
			"tournaments" : util.getAllTournaments(),
		}

	def get( self ):
		return self.getOrPost()

	def post( self ):
		return self.getOrPost()

	def getOrPost( self ):
		raise NotImplementedError()

	def respondWithTemplate( self, templateName ):
		templatePath = os.path.normpath( os.path.join( os.path.dirname( __file__ ), "..", "templates", templateName ) )
		self.response.headers["Content-Type"] = "text/html"
		self.response.out.write( str( template.render( templatePath, self.templateContent ).encode( "ascii", "ignore" ) ) )

	def respondWithError( self, errorMessage ):
		self.templateContent["title"] += " &ndash; Error"
		self.templateContent["errorMessage"] = errorMessage
		self.respondWithTemplate( "Error.tmpl" )

	def getAccountBar( self ):
		user = users.get_current_user()
		if user is not None:
			result = '<ul class="horizontalMenu">\n<li>Signed in as %s</li>\n<li><a href="/editProfile">Edit Profile</a></li>\n<li><a href="%s">Sign out</a></li>\n<li><a href="/help">Help</a></li>\n</ul>' % \
			         (user.nickname(), users.create_logout_url( "/" ))
		else:
			result = '<ul class="horizontalMenu">\n<li><a href="%s">Sign in</a></li>\n<li><a href="/help">Help</a></li>\n</ul>' % users.create_login_url( self.request.uri )
		return result

	def getActionBar( self ):
		lines = ['<ul class="horizontalMenu">']
		actionTuples = [("/", "Leaderboard"),
		                ("/bracket", "Bracket"),
		                ("/matches", "Matches"),
		                ("/submitMatch", "Submit Match")]
		if util.isTournamentAdmin( users.get_current_user(), util.getCurrentTournament() ):
			actionTuples.append( ("/adminTournament", "Administer Tournament") )
		for action, actionName in actionTuples:
			if action == self.request.path:
				lines.append( '<li>%s</li>' % actionName )
			else:
				lines.append( '<li><a href="%s">%s</a></li>' % (action, actionName) )
		lines.append( "</ul>" )
		return "\n".join( lines )

	def getTournamentName( self ):
		if self.currentTournament is None:
			return "None"
		else:
			return self.currentTournament.name

	def getTournamentExtraInfo( self ):
		if self.currentTournament is None:
			return "None"
		else:
			if self.currentTournament.currentStage == "announced":
				return "Tournament has been announced."
			elif self.currentTournament.currentStage == "signUp":
				return "You may sign up for this tournament."
			elif self.currentTournament.currentStage == "preLadder":
				return "Sign up is closed."
			elif self.currentTournament.currentStage == "ladder":
				if self.currentTournament.eliminationStartDatetime is not None:
					endDatetimeString = self.currentTournament.eliminationStartDatetime.strftime( "%A %B %d, %Y, %I:%M %p" )
				else:
					endDatetimeString = "None"
				return "Ladder Round ends: %s" % endDatetimeString
			elif self.currentTournament.currentStage == "preElimination":
				return "Ladder Round has ended."
			elif self.currentTournament.currentStage == "elimination":
				return "Elimination Round is open."
			elif self.currentTournament.currentStage == "finished":
				return "Tournament is finished."
			else:
				return "Unknown state."


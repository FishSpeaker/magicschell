# Copyright (c) 2009-2010 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging

from google.appengine.ext import db
from google.appengine.api import users

from handlers.AddOrEditTournamentHandler import AddOrEditTournamentHandler
from models import Tournament
import util

class EditTournamentHandler( AddOrEditTournamentHandler ):
	def getOrPost( self ):
		if users.get_current_user() is None:
			self.redirect( users.create_login_url( self.request.uri ) )
		elif not users.is_current_user_admin():
			self.baseInit()
			self.templateContent["title"] = "Edit Tournament"
			self.respondWithError( "Only Admins can edit Tournaments" )
		else:
			self.baseInit()
			self.templateContent["title"] = "Edit Tournament"

			# Get tournament to edit.
			tournament = None
			if "key" in self.request.params:
				tournamentKey = self.request.params["key"]
				try:
					tournament = db.get( db.Key( tournamentKey ) )
					if not isinstance( tournament, Tournament ):
						tournament = None
						errorMessage = "Key (%s) does not represent a Tournament." % tournamentKey
				except db.BadKeyError:
					errorMessage = "Illegal Tournament key: %s." % tournamentKey
			else:
				errorMessage = "No tournament key given."

			if tournament is None:
				self.respondWithError( errorMessage )
			else:
				if self.request.get( "submitTournament" ) == "true":
					# New tournament info submitted.
					params, errors = self.validateParams()
					self.templateContent["errors"] = errors
					if len( errors ) == 0:
						# Update tournament.
						for property, value in params["tournamentKeywords"].items():
							setattr( tournament, property, value )
						tournament.put()

						# Set Team Info
						self.updateTeamInfo( tournament, params["teamNamesToUsers"] )

						# Redirect to AdminTournament page
						self.redirect( "/adminTournament?key=%s" % tournament.key() )
					else:
						# Errors encountered.
						self.fillForm( self.request.params )
						self.respondWithTemplate( "AddOrEditTournament.tmpl" )
				else:
					# Fill form with current tournament data.
					teamLines = []
					for team in tournament.teams:
						lineParts = [team.name]
						for playerAssociation in team.playerAssociations:
							lineParts.append( playerAssociation.player.owner.email() )
						teamLines.append( " | ".join( sorted( lineParts ) ) )
					teamInfo = "\n".join( sorted( teamLines ) )

					params = {
						"key" : tournament.key(),
						"name" : util.getEmptyStringForNone( tournament.name ),
						"description" : util.getEmptyStringForNone( tournament.description ),
						"adminEmails" : util.getEmptyStringForNone( "\n".join( [user.email() for user in tournament.admins] ) ),
						"maxBoostersPerTeam" : tournament.maxBoostersPerTeam,
						"opponentsPerBooster" : tournament.opponentsPerBooster,
						"playersPerTeam" : tournament.playersPerTeam,
						"teamInfo" : teamInfo,
						"initialRating" : util.getEmptyStringForNone( tournament.initialRating ),
						"scale" : util.getEmptyStringForNone( tournament.scale ),
						"maxRatingChange" : util.getEmptyStringForNone( tournament.maxRatingChange ),
					}

					self.fillForm( params )
					self.respondWithTemplate( "AddOrEditTournament.tmpl" )

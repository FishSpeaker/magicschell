# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import random

from google.appengine.ext import db
from google.appengine.api import users

from BaseHandler import BaseHandler
from models import Player
from models import Tournament
import util

class AdminHandler( BaseHandler ):
	def getOrPost( self ):
		if users.get_current_user() is None:
			self.redirect( users.create_login_url( self.request.uri ) )
		elif not users.is_current_user_admin():
			self.error( 403 )
		else:
			self.baseInit()
			self.templateContent["title"] = "Admin"
			self.templateContent["result"] = "No result."

			self.respondWithTemplate( "Admin.tmpl" )


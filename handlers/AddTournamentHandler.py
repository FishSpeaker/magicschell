# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import datetime

from google.appengine.api import users

from handlers.AddOrEditTournamentHandler import AddOrEditTournamentHandler
from models import Tournament
import util

class AddTournamentHandler( AddOrEditTournamentHandler ):
	def getOrPost( self ):
		if users.get_current_user() is None:
			self.redirect( users.create_login_url( self.request.uri ) )
		elif not users.is_current_user_admin():
			self.baseInit()
			self.templateContent["title"] = "Edit Tournament"
			self.respondWithError( "Only Admins can create Tournaments" )
		else:
			self.baseInit()
			self.templateContent["title"] = "Create Tournament"
			if self.request.get( "submitTournament" ) == "true":
				params, errors = self.validateParams()
				self.templateContent["errors"] = errors
				if len( errors ) == 0:
					# Check for name duplication.
					# TODO: test for name duplication in a transaction?  (doesn't seem possible)
					existingTournament = Tournament.all().filter( "name =", params["tournamentKeywords"]["name"] ).get()
					if existingTournament is None:
						# Add final pieces to tournamentKeywords.
						params["tournamentKeywords"]["creator"] = users.get_current_user()
#						params["tournamentKeywords"]["admins"] = [users.get_current_user()]
						params["tournamentKeywords"]["eliminationStartDatetime"] = datetime.datetime.today() + datetime.timedelta( days=25 )
						params["tournamentKeywords"]["eliminationStartDatetime"] = params["tournamentKeywords"]["eliminationStartDatetime"].replace( minute=0 )
						params["tournamentKeywords"]["currentStage"] = "announced"

						# Create tournament.
						tournament = Tournament( **params["tournamentKeywords"] )
						tournamentKey = tournament.put()

						# Set Team Info
						self.updateTeamInfo( tournament, params["teamNamesToUsers"] )

						# Redirect to admin page.
						self.redirect( "/adminTournament?key=%s" % tournamentKey )
					else:
						self.fillForm( self.request.params )
						self.templateContent["errors"] = ["A tournament with that name already exists."]
						self.respondWithTemplate( "AddOrEditTournament.tmpl" )
				else:
					self.fillForm( self.request.params )
					self.respondWithTemplate( "AddOrEditTournament.tmpl" )
			else:
				self.fillForm( self.request.params )
				self.respondWithTemplate( "AddOrEditTournament.tmpl" )




# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import traceback

from google.appengine.ext import db
from google.appengine.api import users

from BaseHandler import BaseHandler
from models import Tournament
from models import Player
import util

class AjaxHandler( BaseHandler ):
	def __init__( self ):
		BaseHandler.__init__( self )
		self.templateContent = {}
		self.methods = AjaxMethods()

   	def getOrPost( self ):
		function = None

		functionName = self.request.get( "functionName" )
		if functionName:
			if functionName[0] == "_":
				self.error( 403 ) # access denied
				return
			else:
				function = getattr( self.methods, functionName, None )

		if function is None:
			self.error( 404 ) # function not found
			return

		function( self, self.request.params )

class AjaxMethods:
	def getPlayerOptions( self, ajaxHandler, params ):
		"""
		Returns player options html content based on a selected ladder and
		selected player.

		Input params:
		-- selectedTournament: currently selected ladder key
		-- selectedPlayer: current selected player key
		"""
		# Validate params.
		try:
			if params["selectedTournament"] != "selectTournament":
				selectedTournament = db.get( db.Key( params["selectedTournament"] ) )
				if not isinstance( selectedTournament, Tournament ):
					raise Error( "Bad selectedTournament: %s.  Not a Tournament key." % params["selectedTournament"] )
			else:
				selectedTournament = None

			if params["selectedPlayer"] != "selectPlayer":
				selectedPlayer = db.get( db.Key( params["selectedPlayer"] ) )
				if not isinstance( selectedPlayer, Player ):
					raise Error( "Bad selectedPlayer: %s.  Not a Player key." % params["selectedPlayer"] )
			selectedPlayerKey = params["selectedPlayer"]
		except:
			logging.error( traceback.format_exc() )
			ajaxHandler.error( 400 ) # bad syntax
			return

		# Generate content.
		currentPlayer = util.getCurrentPlayer()
		allPlayers = Player.all().fetch( 1000 )
		ajaxHandler.response.headers["Content-Type"] = "text/html"
		ajaxHandler.response.out.write( util.getPlayerOptions( True, currentPlayer, allPlayers, selectedPlayerKey, selectedTournament=selectedTournament ) )

	def updateResultsPerPage( self, ajaxHandler, params ):
		try:
			player = db.get( db.Key( params["playerKey"] ) )
			if not isinstance( player, Player ):
				raise Error( "Bad player: %s.  Not a Player key." % params["playerKey"] )

			newResultsPerPage = util.parseInt( params["newResultsPerPage"], minimum=10, maximum=100, allowBlank=False )

			settings = util.getOrCreatePlayerSettings( player )
			logging.debug( "settings 1: %s" % settings )
			settings["resultsPerPage"] = newResultsPerPage
			logging.debug( "settings 2: %s" % settings )
			util.setPlayerSettings( player, settings )
			ajaxHandler.response.clear()
			ajaxHandler.response.set_status( 204 ) # no content response code
		except:
			logging.error( traceback.format_exc() )
			ajaxHandler.error( 400 ) # bad syntax




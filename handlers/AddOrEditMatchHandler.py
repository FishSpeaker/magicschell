# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import datetime
import traceback
import re
import operator

from google.appengine.ext import db
from google.appengine.api import users

from BaseHandler import BaseHandler
from models import Tournament
from models import Player
import util

class AddOrEditMatchHandler(BaseHandler):
	def fillForm(self, params, bracketOpponent=None):
		# TODO: handle multiple tournaments
		tournament = util.getCurrentTournament()
		if tournament is None:
			self.templateContent["noData"] = True
		else:
			self.templateContent["noData"] = False

			# Setup defaults
			currentPlayer = util.getCurrentPlayer() # Should not be None because user must be logged in to add or edit Match
			self.templateContent["currentParams"] = {
				"tournament" : str(tournament.key()),
				"player1" : str(currentPlayer.key()),
				"player2" : "selectPlayer",
			}

			# Overwrite from params.
			self.templateContent["currentParams"].update(params)

			# Handle elimination round differences
			if tournament.currentStage == "elimination":
				self.templateContent["isEliminationRoundActive"] = True
				if bracketOpponent is None:
					bracketOpponent = util.getBracketOpponent(currentPlayer, tournament)
				self.templateContent["currentParams"]["player2"] = str(bracketOpponent.key())
				self.templateContent["player2Repr"] = util.getDisplayReprDict(bracketOpponent)["repr"]
			else:
				self.templateContent["isEliminationRoundActive"] = False

			# Generate player options.
			if util.isTournamentAdmin(users.get_current_user(), tournament):
				self.templateContent["isAdmin"] = True
				players = sorted(util.getPlayersInTournament(tournament), cmp=util.playerCmp)
				self.templateContent["player1Options"] = util.getPlayerOptions(True, players, self.templateContent["currentParams"]["player1"])
			else:
				self.templateContent["isAdmin"] = False
				currentTeam = util.getTeamFromPlayerAndTournament(currentPlayer, tournament)
				players = sorted(util.getPlayersInTournament(tournament, excludeTeam=currentTeam), cmp=util.playerCmp)
				self.templateContent["player1Repr"] = util.getDisplayReprDict(currentPlayer)["repr"]
			self.templateContent["player2Options"] = util.getPlayerOptions(True, players, self.templateContent["currentParams"]["player2"])

	def validateParams(self):
		# Input params:
		# -- tournament : string (valid tournament key)
		# -- player1 : string (valid player key)
		# -- player2 : string (valid player key that's not player1 and not on player1's team; or is correct bracket opponent)
		# -- player1Score : float
		# -- player2Score : float

		# Output params:
		# -- matchKeywords : {}

		params = {
			"matchKeywords" : {},
		}
		errors = []

		# tournament
		tournament = None
		if "tournament" in self.request.params:
			tournamentKey = self.request.params["tournament"]
			if tournamentKey != "selectTournament":
				try:
					tournament = db.get(db.Key(tournamentKey))
					if isinstance(tournament, Tournament):
						params["matchKeywords"]["tournament"] = tournament
					else:
						errors.append("Key (%s) does not represent a Tournament." % tournamentKey)
				except db.BadKeyError:
					errors.append("Illegal Tournament key: %s." % tournamentKey)
			else:
				errors.append("No tournament specified.")
		else:
			errors.append("No tournament specified.")

		# players
		for playerParam, prettyName in (("player1", "Player 1"), ("player2", "Player 2")):
			if playerParam in self.request.params:
				playerKey = self.request.params[playerParam]
				if playerKey != "selectPlayer":
					try:
						player = db.get(db.Key(playerKey))
						if isinstance(player, Player):
							if util.isPlayerInTournament(player, tournament):
								params["matchKeywords"][playerParam] = player
							else:
								errors.append("Player (%s) is not registered for this tournament." % player.owner.email())
						else:
							errors.append("%s: Key (%s) does not represent a Player." % (prettyName, playerKey))
					except db.BadKeyError:
						errors.append("%s: Illegal Player key: %s." % (prettyName, playerKey))
				else:
					errors.append("%s not selected." % prettyName)
			else:
				errors.append("%s not selected." % prettyName)

		# Check for identical players.
		if ("player1" in params["matchKeywords"]) and ("player2" in params["matchKeywords"]):
			if params["matchKeywords"]["player1"].key() == params["matchKeywords"]["player2"].key():
				errors.append("Player 1 and Player 2 must be different.")

		# Check for same team.
		if ("player1" in params["matchKeywords"]) and ("player2" in params["matchKeywords"]) and (tournament is not None):
			team1 = util.getTeamFromPlayerAndTournament(params["matchKeywords"]["player1"], tournament)
			team2 = util.getTeamFromPlayerAndTournament(params["matchKeywords"]["player2"], tournament)
			logging.debug("team1: %s" % team1.name)
			logging.debug("team2: %s" % team2.name)
			if team1.key() == team2.key():
				errors.append("Player 1 and Player 2 must be on different teams.")

		# scores
		for scoreParam, prettyName in (("player1Score", "Player 1 score"), ("player2Score", "Player 2 score")):
			if scoreParam in self.request.params:
				scoreString = self.request.params[scoreParam].strip()
				if len(scoreString) > 0:
					try:
						params["matchKeywords"][scoreParam] = util.parseFloat(scoreString, allowBlank=False)
					except util.ParseError:
						errors.append("%s is not a number." % prettyName)
				else:
					errors.append("%s not given." % prettyName)
			else:
				errors.append("%s not given." % prettyName)

		return params, errors

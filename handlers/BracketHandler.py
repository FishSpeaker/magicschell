# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import operator
import random
import os

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext.webapp import template

from handlers.BaseHandler import BaseHandler
from models import Tournament
import util

class BracketHandler( BaseHandler ):
	def getOrPost( self ):
		self.baseInit()
		self.templateContent["title"] = "Bracket"

		tournament = util.getCurrentTournament()
		numRounds, bracketTree = util.getBracketData( tournament )
		totalColumns = 2**(numRounds + 1) - 1

		lines = []
		for level in range( numRounds + 1 ):
			logging.debug( "level: %s" % level )
			columnsPerTeam = 2**(numRounds + 1 - level) - 1
			teamColumns = [i*(columnsPerTeam + 1) for i in range( 2**level )]
			vsColumns = [i*(columnsPerTeam + 1) + columnsPerTeam for i in range( 2**level - 1 ) if i % 2 == 0]
			vsPlaceholderColumns = [i*(columnsPerTeam + 1) + columnsPerTeam for i in range( 2**level - 1 )]

			nodes = bracketTree.getNodesAtLevel( level )

			# Team row
			lines.append( "\t<tr>" )
			nodeIndex = 0
			for i in range( totalColumns ):
				if i in teamColumns:
					node = nodes[nodeIndex]
					nodeIndex += 1
					if node.bye == True:
						teamRepr = "[BYE]"
					elif node.team is None:
						teamRepr = "???"
					else:
						teamRepr = node.team.name
					if level == 0:
						teamRepr = "Winners: " + teamRepr
					if node.wins is not None:
						teamRepr += " (%d)" % sum( node.wins )
					lines.append( '\t\t<td class="teamCell" colspan="%s">%s</td>' % (columnsPerTeam, teamRepr) )
				elif i in vsPlaceholderColumns:
					lines.append( '\t\t<td class="vsCell"></td>' )
			lines.append( "\t</tr>" )

			# Player rows
			for playerIndex in range( tournament.playersPerTeam ):
				lines.append( "\t<tr>" )
				nodeIndex = 0
				for i in range( totalColumns ):
					if i in teamColumns:
						node = nodes[nodeIndex]
						nodeIndex += 1
						if node.bye == True:
							playerRepr = "[BYE]"
						elif node.players is None:
							playerRepr = "?"
						else:
							playerName = util.getPlayerShortRepr( node.players[playerIndex] )
							if node.wins is not None:
								playerRepr = "%s (%d)" % (playerName, node.wins[playerIndex])
							else:
								playerRepr = "%s" % playerName
						lines.append( '\t\t<td class="playerCell" colspan="%s">%s</td>' % (columnsPerTeam, playerRepr) )
					elif i in vsColumns:
						lines.append( '\t\t<td class="vsCell">vs.</td>' )
					elif i in vsPlaceholderColumns:
						lines.append( '\t\t<td class="vsCell"></td>' )
				lines.append( "\t</tr>" )

		self.templateContent["bracketTableContents"] = "\n".join( lines )

		self.respondWithTemplate( "Bracket.tmpl" )


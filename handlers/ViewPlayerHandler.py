# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import operator
import random
import os

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext.webapp import template

from handlers.ViewHandler import ViewHandler
from handlers.PagingHandler import PagingHandler
from models import Player
import util

class ViewPlayerHandler( ViewHandler, PagingHandler ):
	def getOrPost( self ):
		self.baseInit()
		self.templateContent["title"] = "View Player"
		params, errors = self.validateParams()
		self.templateContent.update( params )
		self.templateContent["errors"] = errors
		self.respondWithTemplate( "ViewPlayer.tmpl" )

	def validateParams( self ):
		# Input params:
		# -- playerKey : playerKey
		# -- resultsPerPage : int (10-100)
		# -- startIndex : int (1-991)

		# Output params:
		# -- currentParams : {playerKey, resultsPerPage, startIndex}
		# -- playerRepr : from util.getDisplayReprDict( player )
		# -- playerRating : rating
		# -- teamLink : url
		# -- teamRepr : str
		# -- opponentsPlayed : []
		# -- opponentsNotPlayed : []
		# -- matchInfos : [{dateSubmitted,
		#                   timeSubmitted,
		#                   type,
		#                   winner,
		#                   player1Link,
		#                   player1Repr,
		#                   team1Link,
		#                   team1Repr,
		#                   player1Score,
		#                   player2Link,
		#                   player2Repr,
		#                   team2Link,
		#                   team2Repr,
		#                   player2Score}, ...]
		# -- pagingBar : <html>
		# -- resultsPerPageWidget : <html>

		params = {}
		errors = []
		tournament = util.getCurrentTournament()
		currentPlayerSettings = util.getCurrentPlayerSettings()

		## Input parameters

		# playerKey
		playerKey = self.request.params["playerKey"]
		player = None
		try:
			player = db.get( db.Key( playerKey ) )
			if isinstance( player, Player ):
				if not util.isPlayerInTournament( player, tournament ):
					errors.append( "Player (%s) is not registered for this tournament." % player.owner.email() )
			else:
				errors.append( "%s: Key (%s) does not represent a Player." % (prettyName, playerKey) )
		except db.BadKeyError:
			errors.append( "%s: Illegal Player key: %s." % (prettyName, playerKey) )

		# paging parameters
		resultsPerPage, startIndex = self.validateResultsPerPageAndStartIndex( self.request, currentPlayerSettings )

		## Output parameters

		# currentParams : {playerKey, resultsPerPage, startIndex}
		params["currentParams"] = {"playerKey" : playerKey,
		                           "resultsPerPage" : resultsPerPage,
		                           "startIndex" : startIndex}

		# playerRepr
		params["playerRepr"] = util.getDisplayReprDict( player )

		# playerRating
		params["playerRating"] = "%.2f" % util.getOrCreatePlayerRating( player, tournament ).rating

		# teamLink, teamRepr
		team = util.getTeamFromPlayerAndTournament( player, tournament )
		params["teamLink"], params["teamRepr"] = util.getTeamLinkAndRepr( team )

		# opponentsPlayed, opponentsNotPlayed
		params["opponentsPlayed"] = map( util.getPlayerLinkAndRepr, sorted( util.getPlayersFromKeys( util.getUniqueLadderOpponents( player, tournament ) ), util.playerCmp ) )
		params["opponentsNotPlayed"] = map( util.getPlayerLinkAndRepr, sorted( util.getPlayersFromKeys( util.getUnplayedLadderOpponents( player, tournament ) ), util.playerCmp ) )

		# matchInfos
		query = player.matches.filter( "tournament =", tournament ).order( "-datetimeSubmitted" )
		queryCount = query.count()
		params["matchInfos"] = util.getMatchInfos( query, queryCount, startIndex, resultsPerPage )

		# pagingBar
		pageLink = "?playerKey=%s" % playerKey
		pageLink += "&startIndex=%d"
		if "resultsPerPage" not in currentPlayerSettings:
			pageLink += "&resultsPerPage=%d" % resultsPerPage

		params["pagingBar"] = util.getPagingBar( queryCount, resultsPerPage, startIndex, pageLink )

		# resultsPerPageWidget
		selectName = "resultsPerPage"
		currentPlayer = util.getCurrentPlayer()
		newPageLink = "?playerKey=%s&resultsPerPage=" % playerKey
		params["resultsPerPageWidget"] = util.getResultsPerPageWidget( selectName, resultsPerPage, currentPlayer, newPageLink )

		return params, errors

# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging
import operator
import random
import os

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext.webapp import template

import util

class PagingHandler:
	def validateResultsPerPageAndStartIndex( self, request, currentPlayerSettings ):
		# Results per page.
		resultsPerPage = None
		if "resultsPerPage" in request.params:
			try:
				resultsPerPage = util.parseInt( request.params["resultsPerPage"], minimum=10, maximum=100, allowBlank=False )
			except util.ParseError:
				resultsPerPage = None
		if resultsPerPage is None:
			if "resultsPerPage" in currentPlayerSettings:
				resultsPerPage = currentPlayerSettings["resultsPerPage"]
			else:
				resultsPerPage = 10

		# Start index.
		if "startIndex" in request.params:
			try:
				startIndex = util.parseInt( request.params["startIndex"], minimum=0, maximum=990, allowBlank=False )
				startIndex = startIndex - (startIndex % resultsPerPage)
			except util.ParseError:
				startIndex = 0
		else:
			startIndex = 0

		return (resultsPerPage, startIndex)


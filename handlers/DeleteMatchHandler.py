# Copyright (c) 2014 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

from google.appengine.ext import db
from google.appengine.api import users

from BaseHandler import BaseHandler
from models import Match
import util

class DeleteMatchHandler( BaseHandler ):
	def getOrPost( self ):
		if users.get_current_user() is None:
			self.redirect( users.create_login_url( self.request.uri ) )
		else:
			self.baseInit()
			self.templateContent["title"] = "Delete Match"
			params, errors = self.validateParams()
			self.templateContent.update( params )
			self.templateContent["errors"] = errors

			if( len( errors ) > 0 ):
				self.respondWithError( errors[0] )
			else:
				match = params["match"]
				if util.isTournamentAdmin( users.get_current_user(), match.tournament ) or users.is_current_user_admin():
					if( params["isConfirmed"] ):
						util.deleteMatch( match )
					self.respondWithTemplate( "DeleteMatch.tmpl" )
				else:
					self.respondWithError( "Only Admins can delete matches." )

	def validateParams( self ):
		# Input params:
		# -- match : string (valid match key)
		# -- isConfirmed : bool

		# Output params:
		# -- match : Match
		# -- matchInfo : {dateSubmitted,
		#                 timeSubmitted,
		#                 type,
		#                 winner,
		#                 player1Link,
		#                 player1Repr,
		#                 team1Link,
		#                 team1Repr,
		#                 player1Score,
		#                 player2Link,
		#                 player2Repr,
		#                 team2Link,
		#                 team2Repr,
		#                 player2Score,
		#                 matchKey}
		# -- isConfirmed : bool

		params = {
			"match" : None,
			"matchInfo" : None,
			"isConfirmed" : False,
		}
		errors = []

		# match
		if "match" in self.request.params:
			matchKey = self.request.params["match"]
			try:
				match = db.get( db.Key( matchKey ) )
				if isinstance( match, Match ):
					match2 = util.getDuplicateMatch( match )
					if( match2 is not None ):
						params["match"] = match
						params["matchInfo"] = util.getMatchInfo( match )
					else:
						errors.append( "Could not find duplicate match for (%s)." % matchKey )
				else:
					errors.append( "Key (%s) does not represent a Match." % matchKey )
			except db.BadKeyError:
				errors.append( "Illegal Match key: %s." % matchKey )
		else:
			errors.append( "No match specified." )

		# isConfirmed
		if ("isConfirmed" in self.request.params) and (self.request.params["isConfirmed"] == "true"):
			params["isConfirmed"] = True

		return params, errors

# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import logging

from google.appengine.api import users

from BaseHandler import BaseHandler

class HelpHandler( BaseHandler ):
	def getOrPost( self ):
		self.baseInit()
		self.templateContent["title"] = "Help"
		self.templateContent["signInLink"] = users.create_login_url( self.request.uri )
		self.respondWithTemplate( "Help.tmpl" )


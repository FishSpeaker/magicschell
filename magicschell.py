# Copyright (c) 2009-2010 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import os.path
import logging
import wsgiref.handlers

from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

#from google.appengine.dist import use_library
#use_library('django', '0.96')

import templateExtras
templateExtras.installAlternateBlockRender()  # Slight modification to Django templates.
#template.register_template_library( "templateExtras" )

from handlers.LeaderboardHandler import LeaderboardHandler
from handlers.BracketHandler import BracketHandler
from handlers.MatchesHandler import MatchesHandler
from handlers.AddTournamentHandler import AddTournamentHandler
from handlers.EditTournamentHandler import EditTournamentHandler
from handlers.AdminTournamentHandler import AdminTournamentHandler
from handlers.AddMatchHandler import AddMatchHandler
from handlers.DeleteMatchHandler import DeleteMatchHandler
from handlers.EditProfileHandler import EditProfileHandler
from handlers.ViewPlayerHandler import ViewPlayerHandler
from handlers.HelpHandler import HelpHandler
from handlers.AjaxHandler import AjaxHandler
from handlers.AdminHandler import AdminHandler

def main():
	logging.getLogger().setLevel( logging.DEBUG )
	application = webapp.WSGIApplication( [("/", LeaderboardHandler),
	                                       ("/bracket", BracketHandler),
	                                       ("/matches", MatchesHandler),
	                                       ("/createTournament", AddTournamentHandler),
	                                       ("/editTournament", EditTournamentHandler),
	                                       ("/adminTournament", AdminTournamentHandler),
	                                       ("/submitMatch", AddMatchHandler),
	                                       ("/deleteMatch", DeleteMatchHandler),
	                                       ("/editProfile", EditProfileHandler),
	                                       ("/viewPlayer", ViewPlayerHandler),
#	                                       ("/viewTeam", ViewTeamHandler),
	                                       ("/help", HelpHandler),
	                                       ("/ajax", AjaxHandler),
	                                      ],
	                                      debug=True )
	wsgiref.handlers.CGIHandler().run( application )

def profileApp():
	import cProfile
	import pstats
	prof = cProfile.Profile()
	prof.run( "main()" )
	print "<pre>"
	stats = pstats.Stats( prof )
	stats.sort_stats( "cumulative" )
	stats.print_stats( "magicschell", 40 )
	stats.print_callees( "magicschell", 40 )
	stats.print_callers( "magicschell", 40 )
 	print "</pre>"

if __name__ == "__main__":
	main()
#	profileApp()

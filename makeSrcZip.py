# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import os
import os.path
from zipfile import ZipFile
from zipfile import ZIP_DEFLATED
from fnmatch import fnmatch

def makeSrcZip():
	excludes = (
		"*.pyc",
		".project",
		".pydevproject",
		".settings",
		"magicSchellSrc.zip",
	)

	if not os.path.exists( "downloads" ):
		os.makedirs( "downloads" )

	srcZip = ZipFile( "downloads\\magicSchellSrc.zip", "w", ZIP_DEFLATED )

	for dirpath, dirnames, filenames in os.walk( "" ):
		if isNameExcluded( os.path.basename( dirpath ), excludes ):
			continue
		for filename in getNonExcludedNames( filenames, excludes ):
			srcZip.write( os.path.join( dirpath, filename ) )

	srcZip.close()

def getNonExcludedNames( names, excludes ):
	for name in names:
		if not isNameExcluded( name, excludes ):
			yield name

def isNameExcluded( name, excludes ):
	for exclude in excludes:
		if fnmatch( name, exclude ):
			return True
	return False

if __name__ == "__main__":
	makeSrcZip()

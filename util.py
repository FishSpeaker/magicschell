# Copyright (c) 2009-2010 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

import cgi
import copy
import datetime
import logging
import math
import operator
import os
import pickle
import random
import sys

from extlib import postmarkup
postmarkup.LinkTag.annotate_link = lambda self, domain: u""
from extlib import pytz

from google.appengine.api import memcache
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext.webapp import template

from models import Match
from models import Player
from models import PlayerRating
from models import Team
from models import Tournament
import elo

#-----------------------
# Constants
#-----------------------
monthTuples = [("January", 1),
               ("February", 2),
               ("March", 3),
               ("April", 4),
               ("May", 5),
               ("June", 6),
               ("July", 7),
               ("August", 8),
               ("September", 9),
               ("October", 10),
               ("November", 11),
               ("December", 12)]

# TODO: don't hard code number of matches.
numEliminationMatches = 3

#-----------------------
# Datastore helpers
#-----------------------
class RatingsLockedError( Exception ):
	pass

def getOrCreatePlayer( user ):
	player = Player.all().filter( "owner =", user ).get()
	if player is None:
		player = Player( owner=user,
		                 creator=users.get_current_user(),
		                 email=user.email(),
		                 settings=pickle.dumps( {"resultsPerPage" : 10} ) )
		player.put()
	return player

def getOrCreatePlayerRating( player, tournament ):
	rating = PlayerRating.all().filter( "player =", player ).filter( "tournament =", tournament ).get()
	if rating is None:
		rating = PlayerRating( player=player,
		                       tournament=tournament,
		                       rating=tournament.initialRating )
		rating.put()
	return rating

def getCurrentPlayer():
	currentUser = users.get_current_user()
	if currentUser is not None:
		return getOrCreatePlayer( currentUser )
	else:
		return None

def getPlayersFromKeys( playerKeys ):
	players = []
	for playerKey in playerKeys:
		try:
			player = db.get( db.Key( playerKey ) )
			if isinstance( player, Player ):
				players.append( player )
		except db.BadKeyError:
			pass

	return players

def getOrCreatePlayerSettings( player ):
	if (player.settings is None) or (len( player.settings ) == 0):
		setPlayerSettings( player, {} )

	return pickle.loads( player.settings )

def getCurrentPlayerSettings():
	currentPlayer = getCurrentPlayer()
	if currentPlayer is not None:
		currentPlayerSettings = getOrCreatePlayerSettings( currentPlayer )
	else:
		currentPlayerSettings = {}
	return currentPlayerSettings

def setPlayerSettings( player, settings ):
	player.settings = db.Blob( pickle.dumps( settings ) )
	player.put()

def updateRatingsFromMatch( match ):
	if not match.tournament.isRatingUpdateLocked:
		player1Rating = getOrCreatePlayerRating( match.player1, match.tournament )
		player2Rating = getOrCreatePlayerRating( match.player2, match.tournament )
		newPlayer1Rating, newPlayer2Rating = elo.getNewRatings( match.tournament.scale,
		                                                        match.tournament.maxRatingChange,
		                                                        player1Rating.rating,
		                                                        player2Rating.rating,
		                                                        match.player1Score,
		                                                        match.player2Score )
		player1Rating.rating = newPlayer1Rating
		player2Rating.rating = newPlayer2Rating

		player1Rating.put()
		player2Rating.put()

		updateTeamRating( getTeamFromPlayerAndTournament( match.player1, match.tournament ) )
		updateTeamRating( getTeamFromPlayerAndTournament( match.player2, match.tournament ) )
	else:
		raise RatingsLockedError()

def updateTeamRating( team ):
	ratings = []
	for playerTeamAssociation in team.playerAssociations:
		playerRating = getOrCreatePlayerRating( playerTeamAssociation.player, team.tournament )
		ratings.append( playerRating.rating )
	if len( ratings ) > 0:
		team.rating = sum( ratings )/len( ratings )
	else:
		team.rating = 0.0
	team.put()

def recalculateAllRatings( tournament ):
	logging.debug( "recalculateAllRatings" )
	if not tournament.isRatingUpdateLocked:
		# Lock ratings updates
		tournament.isRatingUpdateLocked = True
		tournament.put()

		try:
			# Setup initial ratings
			playerKeysToRatings = {}
			playerKeysToRatingObjects = {}
			for playerRating in tournament.playerRatings.fetch( 1000 ):
				playerKeysToRatings[playerRating.player.key()] = tournament.initialRating
				playerKeysToRatingObjects[playerRating.player.key()] = playerRating

			# Fetch all ladder matches
			matches = []
			batch = tournament.matches.filter( "isDuplicate =", False ).filter( "isValidForLadder =", True ).order( "datetimeSubmitted" ).fetch( 1000 )
			while True:
				matches.extend( batch )
				if len( batch ) == 1000:
					lastDatetime = batch[-1].datetimeSubmitted
					batch = tournament.matches.filter( "isDuplicate =", False ).filter( "isValidForLadder =", True ).filter( "datetimeSubmitted >", lastDatetime ).order( "datetimeSubmitted" ).fetch( 1000 )
				else:
					break

			# Recalculate all ratings
			for match in matches:
				player1Rating = playerKeysToRatings[match.player1.key()]
				player2Rating = playerKeysToRatings[match.player2.key()]
				newPlayer1Rating, newPlayer2Rating = elo.getNewRatings( tournament.scale,
				                                                        tournament.maxRatingChange,
				                                                        player1Rating,
				                                                        player2Rating,
				                                                        match.player1Score,
				                                                        match.player2Score )
				playerKeysToRatings[match.player1.key()] = newPlayer1Rating
				playerKeysToRatings[match.player2.key()] = newPlayer2Rating

			# Commit new ratings to the datastore
			for playerKey in playerKeysToRatingObjects:
				playerKeysToRatingObjects[playerKey].rating = playerKeysToRatings[playerKey]
				playerKeysToRatingObjects[playerKey].put()

			# Recalculate team ratings
			for team in tournament.teams.fetch( 1000 ):
				# TODO: optimize
				updateTeamRating( team )
		finally:
			# Unlock ratings updates
			tournament.isRatingUpdateLocked = False
			tournament.put()
	else:
		raise RatingsLockedError()

def getPlayersInTournament( tournament, excludeTeam=None, sort=None ):
	playerRatings = PlayerRating.all().filter( "tournament =", tournament ).fetch( 1000 )
	allPlayersInTournament = [playerRating.player for playerRating in playerRatings]
	playerKeysToRatings = {}
	for playerRating in playerRatings:
		key = str( playerRating.player.key() )
		rating = playerRating.rating
		playerKeysToRatings[key] = rating

	# Create list.
	if excludeTeam is not None:
		excludedPlayerKeys = [str( playerAssociation.player.key() ) for playerAssociation in excludeTeam.playerAssociations]
		players = [player for player in allPlayersInTournament if str( player.key() ) not in excludedPlayerKeys]
	else:
		players = allPlayersInTournament

	# Sort.
	if sort == "name":
		players.sort( cmp=playerCmp )
	elif sort == "rating":
		def playerRatingCmp( player1, player2 ):
			rating1 = playerKeysToRatings[str( player1.key() )]
			rating2 = playerKeysToRatings[str( player2.key() )]
			return cmp( rating2, rating1 )
		players.sort( cmp=playerRatingCmp )

	return players

def getTeamFromPlayerAndTournament( player, tournament ):
	for playerTeamAssociation in player.teamAssociations:
		if playerTeamAssociation.team.tournament.key() == tournament.key():
			return playerTeamAssociation.team
	return None

def getTeamsAndPlayersDicts( tournament, sortPlayers=False ):
	teamKeysToTeams = {}
	teamKeysToPlayerLists = {}
	playerKeysToPlayers = {}
	playerKeysToTeams = {}
	if sortPlayers:
		playerKeysToRatings = getPlayerKeysToRatings( tournament )
	for team in tournament.teams.fetch( 1000 ):
		players = [playerAssociation.player for playerAssociation in team.playerAssociations.fetch( 1000 )]
		if sortPlayers:
			def playerRatingCmp( player1, player2 ):
				rating1 = playerKeysToRatings.get( player1.key() )
				rating2 = playerKeysToRatings.get( player2.key() )
				return cmp( rating2, rating1 )
			players.sort( cmp=playerRatingCmp )
		teamKeysToTeams[team.key()] = team
		teamKeysToPlayerLists[team.key()] = players
		for player in players:
			playerKeysToPlayers[player.key()] = player
			playerKeysToTeams[player.key()] = team
	return teamKeysToTeams, teamKeysToPlayerLists, playerKeysToPlayers, playerKeysToTeams

def getPlayerKeysToRatings( tournament ):
	playerKeysToRatings = {}
	for playerRating in PlayerRating.all().filter( "tournament =", tournament ).fetch( 1000 ):
		playerKeysToRatings[playerRating.player.key()] = playerRating.rating
	return playerKeysToRatings

def getPlayerKeysToEliminationMatches( tournament, playerKeys=None ):
	if playerKeys is None:
		playerKeys = [player.key() for player in getPlayersInTournament( tournament )]

	# Setup dictionary with empty lists.
	playerKeysToEliminationMatches = {}
	for playerKey in playerKeys:
		playerKeysToEliminationMatches[playerKey] = []

	# Fill dictionary with matches.
	allMatches = Match.all().filter( "isValidForElimination =", True ).filter( "tournament =", tournament ).fetch( 1000 )
	for match in allMatches:
		playerKeysToEliminationMatches[match.player1.key()].append( match )

	return playerKeysToEliminationMatches

def submitMatch( matchKeywords ):
	matchKeywords1 = copy.copy( matchKeywords )
	matchKeywords2 = copy.copy( matchKeywords )

	# Create match.
	matchKeywords1["isDuplicate"] = False
	match = Match( **matchKeywords1 )

	# Create duplicate match.
	matchKeywords2["isDuplicate"] = True
	matchKeywords2["player1"] = matchKeywords1["player2"]
	matchKeywords2["player2"] = matchKeywords1["player1"]
	matchKeywords2["player1Score"] = matchKeywords1["player2Score"]
	matchKeywords2["player2Score"] = matchKeywords1["player1Score"]
	matchKeywords2["team1"] = matchKeywords1["team2"]
	matchKeywords2["team2"] = matchKeywords1["team1"]
	match2 = Match( **matchKeywords2 )

	if matchKeywords["isValidForLadder"]:
		# Clear unique opponents cache for these players if necessary
		cacheKey1 = "uniqueLadderOpponents.%s.%s" % (matchKeywords["player1"].key(), matchKeywords["tournament"].key())
		cacheKey2 = "uniqueLadderOpponents.%s.%s" % (matchKeywords["player2"].key(), matchKeywords["tournament"].key())
		opponents = memcache.get( cacheKey1 )
		if opponents is not None:
			if str( matchKeywords["player2"].key() ) not in opponents:
				# Cache exists and is no longer valid; clear it.
				memcache.delete( cacheKey1 )
				memcache.delete( cacheKey2 )

		# Clear matchesCount caches
		for cacheKey in ("teamLadderMatchesCount.%s" % matchKeywords["team1"].key(),
		                 "teamLadderMatchesCount.%s" % matchKeywords["team2"].key(),
		                 "playerLadderMatchesCount.%s.%s" % (matchKeywords["tournament"].key(), matchKeywords["player1"].key()),
		                 "playerLadderMatchesCount.%s.%s" % (matchKeywords["tournament"].key(), matchKeywords["player2"].key())):
			memcache.delete( cacheKey )

		# Update ratings.
		updateRatingsFromMatch( match )

	# Write match to database.  This is done after updateRatingsFromMatch,
	# in case RatingsLockedError is raised.
	match.put()
	match2.put()

	return match, match2

def getAllTournaments():
	return Tournament.all().order( "-eliminationStartDatetime" ).fetch( 1000 )

def getCurrentTournament():
	currentPlayerSettings = getCurrentPlayerSettings()
	tournament = None
	if (currentPlayerSettings is not None) and ("currentTournamentKey" in currentPlayerSettings):
		try:
			tournament = db.get( db.Key( currentPlayerSettings["currentTournamentKey"] ) )
			if not isinstance( tournament, Tournament ):
				tournament = None
		except db.BadKeyError:
			# TODO: log error
			pass
		except db.BadArgumentError:
			# TODO: log error
			pass
		except db.BadRequestError:
			# This handles keys screwed up by the hrd migration.
			# TODO: log error
			pass

	if tournament is None:
		# Get most recent tournament
		tournament = Tournament.all().order( "-eliminationStartDatetime" ).get()

	return tournament

def setCurrentTournament( player, tournament ):
	settings = getOrCreatePlayerSettings( player )
	settings["currentTournamentKey"] = str( tournament.key() )
	setPlayerSettings( player, settings )

def isPlayerInTournament( player, tournament ):
	playerKey = player.key()
	playerRatings = PlayerRating.all().filter( "tournament =", tournament ).fetch( 1000 )
	for playerRating in playerRatings:
		if playerRating.player.key() == playerKey:
			return True
	return False

def isUserInTournament( user, tournament ):
	return isPlayerInTournament( getOrCreatePlayer( user ), tournament )

def isTournamentAdmin( user, tournament ):
	authorized = False
	if tournament is not None:
		for admin in tournament.admins:
			if admin == user:
				authorized = True
	return authorized

def getUniqueLadderOpponents( player, tournament ):
	cacheKey = "uniqueLadderOpponents.%s.%s" % (player.key(), tournament.key())
	opponents = memcache.get( cacheKey )
	if opponents is None:
		opponents = set()
		for match in player.matches.filter( "isValidForLadder =", True ).filter( "tournament =", tournament ).fetch( 1000 ):
			opponents.add( str( match.player2.key() ) )
		memcache.add( cacheKey, opponents, time=_getStaggeredExpirationTime() )
	return opponents

def getUnplayedLadderOpponents( player, tournament ):
	playedOpponents = getUniqueLadderOpponents( player, tournament )

	team = getTeamFromPlayerAndTournament( player, tournament )
	allPlayers = getPlayersInTournament( tournament, excludeTeam=team )
	allOpponents = set( [str( player.key() ) for player in allPlayers] )

	unplayedOpponents = allOpponents - playedOpponents
	return unplayedOpponents

def getNumUniqueLadderOpponents( player, tournament ):
	return len( getUniqueLadderOpponents( player, tournament ) )

def getTeamNumUniqueOpponents( team ):
	players = [playerAssociation.player for playerAssociation in team.playerAssociations.fetch( 1000 )]
	return sum( [getNumUniqueLadderOpponents( player, team.tournament ) for player in players] )

def getTeamNumLadderMatches( team ):
	cacheKey = "teamLadderMatchesCount.%s" % team.key()
	matchesCount = memcache.get( cacheKey )
	if matchesCount is None:
		matchesCount = team.matches.filter( "isValidForLadder =", True ).count()
		memcache.add( cacheKey, matchesCount, time=_getStaggeredExpirationTime() )
	return matchesCount

def getPlayerNumLadderMatches( player, tournament ):
	cacheKey = "playerLadderMatchesCount.%s.%s" % (tournament.key(), player.key())
	matchesCount = memcache.get( cacheKey )
	if matchesCount is None:
		matchesCount = player.matches.filter( "isValidForLadder =", True ).filter( "tournament =", tournament ).count()
		memcache.add( cacheKey, matchesCount, time=_getStaggeredExpirationTime() )
	return matchesCount

def getBoostersEarned( team ):
	players = [playerAssociation.player for playerAssociation in team.playerAssociations.fetch( 1000 )]
	uniqueLadderOpponents = 0
	for player in players:
		uniqueLadderOpponents += getNumUniqueLadderOpponents( player, team.tournament )

	tournament = team.tournament
	return min( tournament.maxBoostersPerTeam, uniqueLadderOpponents//tournament.opponentsPerBooster )

def getBoostersOwed( team ):
	if team.tournament.ladderStartDate is not None:
		boostersEarned = getBoostersEarned( team )

		weekdaysInTournament = 0
		day = team.tournament.ladderStartDate.weekday()
		for i in range( (datetime.date.today() - team.tournament.ladderStartDate).days + 1 ):
			if day < 5:
				weekdaysInTournament += 1
			day = (day + 1) % 7

		return min( weekdaysInTournament, boostersEarned )
	else:
		return 0

def getMatchInfos( query, queryCount, startIndex, resultsPerPage ):
	matchInfos = []
	if startIndex > queryCount:
		startIndex = max( queryCount - (queryCount % resultsPerPage) + 1, 1 )
	for match in query.fetch( resultsPerPage, offset=startIndex ):
		matchInfos.append( getMatchInfo( match ) )
	return matchInfos

def getMatchInfo( match ):
	reprDict = getDisplayReprDict( match )
	if match.player1Score > match.player2Score:
		winner = "player1"
	elif match.player2Score > match.player1Score:
		winner = "player2"
	else:
		winner = "tie"
	player1Link, player1Repr = getPlayerLinkAndRepr( match.player1 )
	player2Link, player2Repr = getPlayerLinkAndRepr( match.player2 )

	try:
		team1Link, team1Repr = getTeamLinkAndRepr( match.team1 )
	except db.Error:
		# Catching "Error: ReferenceProperty failed to be resolved"
		team1Link, team1Repr = "", "[Missing]"
	try:
		team2Link, team2Repr = getTeamLinkAndRepr( match.team2 )
	except db.Error:
		# Catching "Error: ReferenceProperty failed to be resolved"
		team2Link, team2Repr = "", "[Missing]"

	if match.isValidForLadder:
		matchType = "Ladder"
	elif match.isValidForElimination:
		matchType = "Elimination"
	else:
		matchType = None
	return {"dateSubmitted" : reprDict["dateSubmitted"],
	        "timeSubmitted" : reprDict["timeSubmitted"],
	        "type" : matchType,
	        "winner" : winner,
	        "player1Link" : player1Link,
	        "player1Repr" : player1Repr,
	        "team1Link" : team1Link,
	        "team1Repr" : team1Repr,
	        "player1Score" : reprDict["player1Score"],
	        "player2Link" : player2Link,
	        "player2Repr" : player2Repr,
	        "team2Link" : team2Link,
	        "team2Repr" : team2Repr,
	        "player2Score" : reprDict["player2Score"],
	        "matchKey" : str( match.key() )}

def deleteMatch( match ):
	match2 = getDuplicateMatch( match )
	match.delete()
	match2.delete()

def getDuplicateMatch( match ):
	query = Match.all().filter( "creator =", match.creator ) \
	                   .filter( "datetimeSubmitted >", match.datetimeSubmitted - datetime.timedelta( seconds=5 ) ) \
	                   .filter( "datetimeSubmitted <", match.datetimeSubmitted + datetime.timedelta( seconds=5 ) ) \
	                   .filter( "tournament =", match.tournament ) \
	                   .filter( "isValidForLadder =", match.isValidForLadder ) \
	                   .filter( "isValidForElimination =", match.isValidForElimination ) \
	                   .filter( "player1 =", match.player2 ) \
	                   .filter( "player2 =", match.player1 ) \
	                   .filter( "player1Score =", match.player2Score ) \
	                   .filter( "player2Score =", match.player1Score ) \
	                   .filter( "team1 =", match.team2 ) \
	                   .filter( "team2 =", match.team1 ) \
	                   .filter( "isDuplicate =", not match.isDuplicate )
	if query.count() == 1:
		return query.get()
	else:
		return None

def _getStaggeredExpirationTime():
	# expire in 18 to 30 hours, staggered to avoid daily hitching
	return random.randint( 18*60*60, 30*60*60 )

#-----------------------
# User input parsing
#-----------------------
class ParseError( Exception ):
	pass

def parseInt( input, minimum=-sys.maxint-1, maximum=sys.maxint, allowBlank=True ):
	return parseNum( int, input, minimum, maximum, allowBlank )

def parseFloat( input, minimum=-1e400, maximum=1e400, allowBlank=True ):
	return parseNum( float, input, minimum, maximum, allowBlank )

def parseNum( constructor, input, minimum, maximum, allowBlank ):
	input = input.strip()

	if allowBlank and (len( input ) == 0):
		return None

	try:
		result = constructor( input )
	except ValueError:
		raise ParseError()

	if (result < minimum) or (result > maximum):
		raise ParseError()
	else:
		return result

#-----------------------
# Output formatting
#-----------------------
def getLinkRepr( link ):
	if link is not None:
		return '<a href="%s">%s</a>' % (cgi.escape( link ), cgi.escape( link ))
	else:
		return ""

def getFloatRepr( n, format="%s" ):
	if n is not None:
		if n % 1.0 == 0.0:
			n = int( n )
		return format % n
	else:
		return ""

def getIntRepr( n, format="%s" ):
	if n is not None:
		return format % n
	else:
		return ""

def getDateRepr( d ):
	if isinstance( d, datetime.datetime ):
		d = d.replace( tzinfo=pytz.utc ).astimezone( getTz( "US/Eastern" ) ) # Hard code to Eastern time for now.
	if d is not None:
		return d.strftime( "%B %d, %Y" )
	else:
		return ""

def getTimeRepr( d ):
	if isinstance( d, datetime.datetime ):
		d = d.replace( tzinfo=pytz.utc ).astimezone( getTz( "US/Eastern" ) ) # Hard code to Eastern time for now.
	if d is not None:
		t = d.strftime( "%I:%M%p" )
		if t[0] == "0":
			t = t[1:]
		t = t.lower()
		return t
	else:
		return ""

def getDateTimeRepr( d, format="%B %d, %Y %I:%M%p" ):
	d = d.replace( tzinfo=pytz.utc ).astimezone( getTz( "US/Eastern" ) ) # Hard code to Eastern time for now.
	if d is not None:
		return d.strftime( format )
	else:
		return ""

def getAge( d ):
	if isinstance( d, datetime.datetime ):
		d = d.date()
	today = datetime.date.today()
	if d > today:
		age = 0
	elif today.month > d.month:
		age = today.year - d.year
	elif today.month < d.month:
		age = today.year - d.year - 1
	elif today.day > d.day:
		age = today.year - d.year
	elif today.day < d.day:
		age = today.year - d.year - 1
	else:
		age = today.year - d.year

	return age

def getAgeRepr( d ):
	if isinstance( d, (datetime.datetime, datetime.date) ):
		return "%d" % getAge( d )
	else:
		return ""

def getEmptyStringForNone( item ):
	if item is not None:
		return item
	else:
		return ""

def getDisplayReprDict( entity ):
	result = None
	if isinstance( entity, Tournament ):
		result = {
			"creator" : unicode( entity.creator ),
			"admins" : ", ".join( [user.email() for user in entity.admins] ),
			"name" : entity.name,
			"description" : postmarkup.render_bbcode( entity.description ),
			"maxBoostersPerTeam" : getIntRepr( entity.maxBoostersPerTeam ),
			"opponentsPerBooster" : getIntRepr( entity.opponentsPerBooster ),
			"playersPerTeam" : getIntRepr( entity.playersPerTeam ),
			"initialRating" : getFloatRepr( entity.initialRating ),
			"scale" : getFloatRepr( entity.scale ),
			"maxRatingChange" : getFloatRepr( entity.maxRatingChange ),
			"eliminationStartDatetime" : getDateTimeRepr( entity.eliminationStartDatetime ),
			"currentStage" : str( entity.currentStage ),
		}
	elif isinstance( entity, Player ):
		result = {
			"owner" : unicode( entity.owner ),
			"creator" : unicode( entity.creator ),
			"firstName" : getEmptyStringForNone( entity.firstName ),
			"lastName" : getEmptyStringForNone( entity.lastName ),
			"name" : "%s %s" % (getEmptyStringForNone( entity.firstName ), getEmptyStringForNone( entity.lastName )),
			"email" : unicode( entity.email ),
		}
		result["name"] = result["name"].strip()
		if len( result["email"] ) == 0:
			result["email"] = unicode( entity.owner.email() )
		if len( result["name"] ) > 0:
			result["repr"] = result["name"]
		else:
			result["repr"] = result["email"]
	elif isinstance( entity, Team ):
		result = {
			"name" : getEmptyStringForNone( entity.name ),
#			"tournament" : getDisplayReprDict( entity.tournament ),
			"rating" : getFloatRepr( entity.rating, format="%.0f" ),
			"boostersReceived" : getIntRepr( entity.boostersReceived ),
#			"boostersEarned" : getIntRepr( getBoostersEarned( entity ) ),
#			"players" : [getDisplayReprDict( playerAssociation.player ) for playerAssociation in entity.playerAssociations],
#			"playerReprs" : ", ".join( sorted( [getDisplayReprDict( playerAssociation.player )["repr"] for playerAssociation in entity.playerAssociations] ) ),
		}
	elif isinstance( entity, Match ):
		try:
			team1Repr = str( entity.team1 )
		except db.Error:
			# Catching "Error: ReferenceProperty failed to be resolved"
			team1Repr = "<Missing>"
		try:
			team2Repr = str( entity.team2 )
		except db.Error:
			# Catching "Error: ReferenceProperty failed to be resolved"
			team2Repr = "<Missing>"
		result = {
			"creator" : unicode( entity.creator ),
			"datetimeSubmitted" : getDateTimeRepr( entity.datetimeSubmitted ),
			"dateSubmitted" : getDateRepr( entity.datetimeSubmitted ),
			"timeSubmitted" : getTimeRepr( entity.datetimeSubmitted ),
			"player1" : entity.player1,
			"player2" : entity.player2,
			"team1" : team1Repr,
			"team2" : team2Repr,
			"player1Score" : getFloatRepr( entity.player1Score ),
			"player2Score" : getFloatRepr( entity.player2Score ),
		}
	elif isinstance( entity, PlayerRating ):
		result = {
			"player" : getDisplayReprDict( entity.player ),
			"tournament" : getDisplayReprDict( entity.tournament ),
			"rating" : getFloatRepr( entity.rating, format="%.0f" ),
		}

	if result is not None:
		result["key"] = str( entity.key() )
	return result

def getPlayerShortRepr( player ):
	if (player.firstName is not None) and (len( player.firstName.strip() ) > 0):
		shortRepr = player.firstName.strip()
	else:
		if (player.email is not None) and (len( player.email.strip() ) > 0):
			email = player.email.strip()
		else:
			email = player.owner.email()
		if "@" in email:
			shortRepr = email[:email.find( "@" )]
		else:
			shortRepr = email
	return shortRepr

def getPlayerLinkAndShortRepr( player ):
	link = "/viewPlayer?playerKey=%s" % player.key()
	return (link, getPlayerShortRepr( player ))

def getPlayerLinkAndRepr( player ):
	link = "/viewPlayer?playerKey=%s" % player.key()
	repr = getDisplayReprDict( player )["repr"]
	return (link, repr)

def getTeamLinkAndRepr( team ):
	if team is None:
		return ("/viewTeam?teamKey=null", "<null>")
	else:
		link = "/viewTeam?teamKey=%s" % team.key()
		return (link, team.name)

#-----------------------
# Content generators
#-----------------------
def getPagingBar( queryCount, resultsPerPage, startIndex, pageLink, numSurroundingPages=3, minPagesToCollapse=3 ):
	# there should be at least two pages
	if queryCount <= resultsPerPage:
		return ""

	# setup variables
	lastPage, remainder = divmod( queryCount, resultsPerPage )
	if remainder > 0:
		lastPage += 1
	currentPage = (startIndex//resultsPerPage) + 1
	firstSurroundingPage = currentPage - numSurroundingPages
	if firstSurroundingPage <= 1 + minPagesToCollapse:
		firstSurroundingPage = 2
	lastSurroundingPage = currentPage + numSurroundingPages
	if lastSurroundingPage >= lastPage - minPagesToCollapse:
		lastSurroundingPage = lastPage - 1
	if lastSurroundingPage < currentPage:
		lastSurroundingPage = currentPage

	# generate page numbers
	pageNumbers = [1]
	if currentPage > 1:
		if firstSurroundingPage > 2:
			pageNumbers.append( "ellipsis" )
		pageNumbers.extend( range( firstSurroundingPage, currentPage + 1 ) )
	if currentPage < lastSurroundingPage:
		pageNumbers.extend( range( currentPage + 1, lastSurroundingPage + 1 ) )
	if lastSurroundingPage < lastPage - 1:
		pageNumbers.append( "ellipsis" )
	if lastSurroundingPage < lastPage:
		pageNumbers.append( lastPage )

	# create context
	context = {}
	if currentPage > 1:
		context["hasPreviousLink"] = True
		context["previousLink"] = pageLink % (startIndex - resultsPerPage)
	else:
		context["hasPreviousLink"] = False

	pageLinks = []
	for pageNumber in pageNumbers:
		if pageNumber == "ellipsis":
			pageLinks.append( "ellipsis" )
		else:
			pageLinks.append( {"isCurrentPage" : pageNumber == currentPage,
			                   "pageNumber" : pageNumber,
			                   "link" : pageLink % (resultsPerPage*(pageNumber - 1))} )
	context["pageLinks"] = pageLinks

	if currentPage < lastPage:
		context["hasNextLink"] = True
		context["nextLink"] = pageLink % (startIndex + resultsPerPage)
	else:
		context["hasNextLink"] = False

	templatePath = os.path.normpath( os.path.join( os.path.dirname( __file__ ), "templates", "pagingBar.tmpl" ) )
	return str( template.render( templatePath, context ) )

def getResultsPerPageWidget( selectName, resultsPerPage, currentPlayer, newPageLink ):
	options = [10, 25, 50, 100]
	if resultsPerPage not in options:
		options.append( resultsPerPage )
		options.sort()

	context = {}
	context["name"] = selectName
	if currentPlayer is None:
		context["currentPlayerExists"] = "false"
	else:
		context["currentPlayerExists"] = "true"
		context["currentPlayerKey"] = str( currentPlayer.key() )
	context["newPageLink"] = newPageLink
	context["resultsPerPageOptions"] = options
	context["selectedResultsPerPage"] = resultsPerPage

	templatePath = os.path.normpath( os.path.join( os.path.dirname( __file__ ), "templates", "resultsPerPageWidget.tmpl" ) )
	return str( template.render( templatePath, context ) )

def getPlayerOptions( useSelectPlayer, players, selectedPlayerKey ):
#	logging.debug( "useSelectPlayer: %s" % useSelectPlayer )
#	logging.debug( "players: %s" % players )
#	logging.debug( "selectedPlayerKey: %s" % selectedPlayerKey )

	# Setup template context
	context = {}
	context["useSelectPlayer"] = useSelectPlayer
	context["players"] = [getDisplayReprDict( player ) for player in players]
	context["selectedPlayer"] = selectedPlayerKey

	# Generate content.
	templatePath = os.path.normpath( os.path.join( os.path.dirname( __file__ ), "templates", "playerOptions.tmpl" ) )
	return str( template.render( templatePath, context ) )

#-----------------------
# Bracket Stuff
#-----------------------
class BracketNode:
	def __init__( self ):
		self.team = None
		self.players = None
		self.wins = None
		self.bye = False
		self.leftNode = None
		self.rightNode = None

	def getNodesAtLevel( self, n ):
		if n == 0:
			return [self]
		else:
			return self.leftNode.getNodesAtLevel( n - 1 ) + self.rightNode.getNodesAtLevel( n - 1 )

	def __str__( self ):
		lines = []
		self._strWithIndent( 0, lines )
		return "\n".join( lines )

	def _strWithIndent( self, indent, lines ):
		if self.team is None:
			teamName = "None"
		else:
			teamName = self.team.name

		if self.players is None:
			playerNames = "None"
		else:
			playerNames = "[%s]" % ", ".join( [getPlayerShortRepr( player ) for player in self.players] )

		lines.append( "    "*indent + "team: %s" % teamName )
		lines.append( "    "*indent + "players: %s" % playerNames )
		lines.append( "    "*indent + "wins: %s" % self.wins )
		lines.append( "    "*indent + "bye: %s" % self.bye )
		lines.append( "    "*indent + "leftNode:" )
		if self.leftNode is not None:
			self.leftNode._strWithIndent( indent + 1, lines )
		lines.append( "    "*indent + "rightNode:" )
		if self.rightNode is not None:
			self.rightNode._strWithIndent( indent + 1, lines )

def getBracketData( tournament ):
	teamKeysToTeams, teamKeysToPlayerLists, playerKeysToPlayers, playerKeysToTeams = getTeamsAndPlayersDicts( tournament, sortPlayers=True )
	playerKeysToMatches = getPlayerKeysToEliminationMatches( tournament, playerKeys=playerKeysToPlayers.keys() )
	teams = sorted( teamKeysToTeams.values(), key=operator.attrgetter( "rating" ), reverse=True )
	numRounds = int( math.ceil( math.log( len( teams ), 2 ) ) )
	bracketTree = _getBracketTree( 0, 0, numRounds, teams, teamKeysToPlayerLists, playerKeysToMatches )
	return numRounds, bracketTree

def _getBracketTree( position, level, endLevel, teams, teamKeysToPlayerLists, playerKeysToMatches ):
	# Recursively make bracket tree.
	# Make nodes while traversing down.
	node = BracketNode()
	if level == endLevel:
		if position < len( teams ):
			node.team = teams[position]
			node.players = teamKeysToPlayerLists[node.team.key()]
		else:
			node.bye = True
	else:
		nextLevel = level + 1
		opponentPosition = ((2**nextLevel) - 1) - position
		node.leftNode = _getBracketTree( position, nextLevel, endLevel, teams, teamKeysToPlayerLists, playerKeysToMatches )
		node.rightNode = _getBracketTree( opponentPosition, nextLevel, endLevel, teams, teamKeysToPlayerLists, playerKeysToMatches )

		# Fill in data on the way back up.
		if node.leftNode.team is None:
			# No results from children yet.
			pass
		elif node.rightNode.team is None:
			if node.rightNode.bye:
				# Left team is unopposed.
				node.team = node.leftNode.team
				node.players = node.leftNode.players
			else:
				# No result from right team yet.
				pass
		else:
			# Children are determined.  Look for match results.
			numPlayers = len( node.leftNode.players )
			node.leftNode.wins = [0]*numPlayers
			node.rightNode.wins = [0]*numPlayers
			for i in range( numPlayers ):
				leftPlayer = node.leftNode.players[i]
				rightPlayer = node.rightNode.players[i]
				for match in playerKeysToMatches[leftPlayer.key()]:
					if match.player2.key() == rightPlayer.key():
						if match.player1Score > match.player2Score:
							node.leftNode.wins[i] += 1.0
						elif match.player1Score < match.player2Score:
							node.rightNode.wins[i] += 1.0
						else:
							# Handle tie.
							node.leftNode.wins[i] += 0.5
							node.rightNode.wins[i] += 0.5

			leftWins = sum( node.leftNode.wins )
			rightWins = sum( node.rightNode.wins )
			if (leftWins + rightWins) >= numEliminationMatches*numPlayers:  # TODO: don't hard code number of matches.
				# Match has been decided.
				if leftWins > rightWins:
					node.team = node.leftNode.team
					node.players = node.leftNode.players
				elif rightWins > leftWins:
					node.team = node.rightNode.team
					node.players = node.rightNode.players
				else:
					# Can't handle ties here at the moment.
					pass

	return node

def getBracketOpponent( player, tournament, bracketNode=None ):
	if bracketNode is None:
		numRounds, bracketNode = getBracketData( tournament )

	checkLeftNode = False
	checkRightNode = False
	if bracketNode.leftNode is not None:
		if bracketNode.rightNode is not None:
			# Both nodes are valid
			if bracketNode.leftNode.players is not None:
				if bracketNode.rightNode.players is not None:
					# Both nodes have players; look for valid opponent.
					# Only accept if these players haven't finished their games.
					for i in range( tournament.playersPerTeam ):
						if bracketNode.leftNode.players[i].key() == player.key():
							numMatches = bracketNode.leftNode.wins[i] + bracketNode.rightNode.wins[i]
							numMatchesAllPlayers = sum( bracketNode.leftNode.wins ) + sum( bracketNode.rightNode.wins )
							if numMatches < numEliminationMatches:
								# Opponent has been found and there are games
								# left to play.
								return bracketNode.rightNode.players[i]
							elif numMatchesAllPlayers == numEliminationMatches*len( bracketNode.leftNode.players ):
								# Opponent has been found, there are no regular
								# games left to play, but a tie-breaker is needed.
								return bracketNode.rightNode.players[i]
							else:
								# Opponent was found, but all games for this
								# round have been played.
								return None
						elif bracketNode.rightNode.players[i].key() == player.key():
							numMatches = bracketNode.leftNode.wins[i] + bracketNode.rightNode.wins[i]
							numMatchesAllPlayers = sum( bracketNode.leftNode.wins ) + sum( bracketNode.rightNode.wins )
							if numMatches < numEliminationMatches:
								# Opponent has been found and there are games
								# left to play.
								return bracketNode.leftNode.players[i]
							elif numMatchesAllPlayers == numEliminationMatches*len( bracketNode.leftNode.players ):
								# Opponent has been found, there are no regular
								# games left to play, but a tie-breaker is needed.
								return bracketNode.leftNode.players[i]
							else:
								# Opponent was found, but all games for this
								# round have been played.
								return None
					# This node's children had a full set of players, but not
					# the one we were looking for.  No need to recurse.
					return None
				else:
					# Left node had players, but not the right node.
					checkRightNode = True
			elif bracketNode.rightNode.players is not None:
				# Right node had players, but not the left node.
				checkLeftNode = True
			else:
				# Neither node had players.
				checkLeftNode = True
				checkRightNode = True
		else:
			# Left node is valid, but right node is not.  Search further on
			# the left branch.
			checkLeftNode = True
	elif bracketNode.rightNode is not None:
		# Right node is valid, but left node is not.  Search further on
		# the right branch.
		checkRightNode = True

	# Recurse if necessary
	if checkLeftNode:
		leftResult = getBracketOpponent( player, tournament, bracketNode=bracketNode.leftNode )
		if leftResult is not None:
			return leftResult

	if checkRightNode:
		rightResult = getBracketOpponent( player, tournament, bracketNode=bracketNode.rightNode )
		if rightResult is not None:
			return rightResult

	# If we got here, we failed to find an opponent.
	return None

#-----------------------
# Cache clearing
#-----------------------
def clearUniqueLadderOpponentsCache( tournament ):
	for player in getPlayersInTournament( tournament ):
		cacheKey = "uniqueLadderOpponents.%s.%s" % (player.key(), tournament.key())
		memcache.delete( cacheKey )

def clearTeamLadderMatchesCountCache( tournament ):
	for team in tournament.teams.fetch( 1000 ):
		cacheKey = "teamLadderMatchesCount.%s" % team.key()
		memcache.delete( cacheKey )

def clearPlayerLadderMatchesCountCache( tournament ):
	for player in getPlayersInTournament( tournament ):
		cacheKey = "playerLadderMatchesCount.%s.%s" % (tournament.key(), player.key())
		memcache.delete( cacheKey )

#-----------------------
# Sorting functions
#-----------------------
def playerCmp( player1, player2 ):
	return cmp( (player1.firstName, player1.lastName, player1.email), (player2.firstName, player2.lastName, player2.email) )

def playerReprCmp( player1, player2 ):
	return cmp( (player1["firstName"], player1["lastName"], player1["email"]), (player2["firstName"], player2["lastName"], player2["email"]) )

#-----------------------
# Timezone handling
#-----------------------
# Based on http://takashi-matsuo.blogspot.com/2008/07/using-newest-zipped-pytz-on-gae.html
def getTz( tzname ):
	tz = memcache.get( "tz:%s" % tzname )
	if tz is None:
		tz = pytz.timezone( tzname )
		memcache.add( "tz:%s" % tzname, tz, 86400 )

	return tz


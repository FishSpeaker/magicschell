# Copyright (c) 2009-2010 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

from google.appengine.ext import db
from google.appengine.api import users

class Player( db.Model ):
	owner = db.UserProperty()
	creator = db.UserProperty()
	timeCreated = db.DateTimeProperty( auto_now_add=True )
	firstName = db.StringProperty()
	lastName = db.StringProperty()
	email = db.EmailProperty()
	settings = db.BlobProperty() # dictionary of settings

class Tournament( db.Model ):
	creator = db.UserProperty()
	admins = db.ListProperty( users.User )
	name = db.StringProperty()
	description = db.TextProperty()
	playersPerTeam = db.IntegerProperty()
	maxPlayersAllowed = db.IntegerProperty()
	maxBoostersPerTeam = db.IntegerProperty()
	opponentsPerBooster = db.IntegerProperty()
	initialRating = db.FloatProperty()
	scale = db.FloatProperty()
	maxRatingChange = db.FloatProperty()
	ladderStartDate = db.DateProperty()
	eliminationStartDatetime = db.DateTimeProperty()
	currentStage = db.StringProperty() # one of: "announced", "signUp", "preLadder", "ladder", "preElimination", "elimination", "finished"
	isRatingUpdateLocked = db.BooleanProperty( default=False )

class Team( db.Model ):
	name = db.StringProperty()
	tournament = db.ReferenceProperty( Tournament, collection_name="teams" )
	rating = db.FloatProperty()
	boostersReceived = db.IntegerProperty()

class PlayerTeamAssociation( db.Model ):
	player = db.ReferenceProperty( Player, collection_name="teamAssociations" )
	team = db.ReferenceProperty( Team, collection_name="playerAssociations" )

class Match( db.Model ):
	creator = db.UserProperty()
	datetimeSubmitted = db.DateTimeProperty( auto_now_add=True )
	tournament = db.ReferenceProperty( Tournament, collection_name="matches" )
	isValidForLadder = db.BooleanProperty()
	isValidForElimination = db.BooleanProperty()
	player1 = db.ReferenceProperty( Player, collection_name="matches" )
	player2 = db.ReferenceProperty( Player, collection_name="matchesAsPlayer2" )
	player1Score = db.FloatProperty()
	player2Score = db.FloatProperty()
	team1 = db.ReferenceProperty( Team, collection_name="matches" )
	team2 = db.ReferenceProperty( Team, collection_name="matchesAsTeam2" )
	isDuplicate = db.BooleanProperty()

class PlayerRating( db.Model ):
	player = db.ReferenceProperty( Player, collection_name="ratings" )
	tournament = db.ReferenceProperty( Tournament, collection_name="playerRatings" )
	rating = db.FloatProperty()


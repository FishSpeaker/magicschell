# Copyright (c) 2009 Jason Pratt
# This software is distributed under the MIT License, which can be found
# in the LICENSE.txt file accompanying the software, or at this url:
# http://www.opensource.org/licenses/mit-license.html

def getNewRatings( scale, maxRatingChange, player1Rating, player2Rating, player1Score, player2Score ):
	if player1Score > player2Score:
		player1Result = 1.0
	elif player1Score < player2Score:
		player1Result = 0.0
	else:
		player1Result = 0.5
	player1ExpectedValue = 1.0/(1.0 + 10.0**((player2Rating - player1Rating)/scale))
	ratingChange = maxRatingChange*(player1Result - player1ExpectedValue)
	return (player1Rating + ratingChange, player2Rating - ratingChange)



